const axios = require("axios");
module.exports = {
  currency: function (to, from, amount, callback) {
    axios
      .get(
        // `http://free.currconv.com/api/v7/convert?q=${from}_${to}&compact=ultra&apiKey=7fc8399686a3cb3b6168`
          `http://data.fixer.io/api/convert?access_key=038eaaf4bff32fd901ebeab49c79ba0c&from=${from}&to=${to}&amount=${amount}`
        )
      .then(function (response) {
        // var t = `${from}_${to}`;
        // var b = response.data;
        // var New = amount * b[t];
        var New = response.data.result;
        if (callback != null) callback(New);
        console.log({ base: from, to: to, baseAmount: amount, toAmount: New });
      })
      .catch(function (error) {
        console.log(error);

      });
  },
};

// function (to, from ,amount, callback){
//   axios.get(`http://api.exchangeratesapi.io/latest?symbols=${to}&base=${from}`)
//   .then(function (response) {
//     var b = response.data.rates;
//     New = amount * b[to];
//     console.log({base: from, to: to, baseAmount: amount, toAmount: New});
//     if(callback != null)
//     callback(New);

//   })
//   .catch(function (error) {
//     console.log(error);
//   })
//   console.log('after')

//  }
