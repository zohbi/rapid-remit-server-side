var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Setting = new Schema(
  {
    email: {
      type: String,
      required: true,
    },
    companyName: {
      type: String,
      required: true,
    },
    officeAddress: {
      type: String,
      required: true,
    },
    poBox: {
      type: String,
      required: true,
    },
    city: {
      type: String,
      required: true,
    },
    country: {
      type: String,
      required: true,
    },
    mapCoortinates: {
      type: [Number],
      required: true,
    },
    facebookLink: {
      type: String,
      required: true,
    },
    instagramLink: {
      type: String,
      required: true,
    },
    linkedInLink: {
      type: String,
      required: true,
    },
    twitterLink: {
      type: String,
      required: true,
    },
    iosLink: {
      type: String,
      required: true,
    },
    androidLink: {
      type: String,
      required: true,
    },
  },
  {
    timestamps: true,
  }
);
module.exports = mongoose.model('setting', Setting);
