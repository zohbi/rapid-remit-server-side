var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var mvHeading = new Schema(
  {
    our_mission: {
      type: String,
      required: true,
    },
    our_vision: {
      type: String,
      required: true,
    },
  },
  {
    timestamps: true,
  }
);
module.exports = mongoose.model('mvheadings', mvHeading);
