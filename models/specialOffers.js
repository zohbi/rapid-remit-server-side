var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var offersSchema = new Schema(
  {
    title: {
      type: String,
      required: true,
    },

    text: {
      type: String,
      required: true,
    },
    expiryDate: {
      type: Date,
      required: true,
    },
    Selected: {
      type: Boolean,
      default: false,
    },
    img: {
      type: String,
    },
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("specialoffer", offersSchema);
