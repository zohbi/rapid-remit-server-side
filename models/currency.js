var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var currencySchema = new Schema(
  {
    currency_name: {
      type: String,
      required: true,
    },
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model('currency', currencySchema);
