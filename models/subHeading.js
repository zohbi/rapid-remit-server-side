var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var subHeading = new Schema(
  {
    para_1: {
      type: String,
      required: true,
    },
    para_2: {
      type: String,
      required: true,
    },
  },
  {
    timestamps: true,
  }
);
module.exports = mongoose.model('subheadings', subHeading);
