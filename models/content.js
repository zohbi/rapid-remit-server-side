var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var content = new Schema(
  {
    comparison_one: {
      type: String,
    },
    comparison_two: {
      type: String,
    },
  },
  {
    timestamps: true,
  }
);
module.exports = mongoose.model("content", content);
