var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var aboutUsHeading = new Schema(
  {
    heading: {
      type: String,
      required: true,
    },
    sub_heading: {
      type: String,
      required: true,
    },
    text: {
      type: String,
      required: true,
    },
  },
  {
    timestamps: true,
  }
);
module.exports = mongoose.model('aboutusheadings', aboutUsHeading);
