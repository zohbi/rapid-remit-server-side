var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var feedbackSchema = new Schema({
    title:{
        type:String,
        required:true
    },
    
    text:{
        type:String,
        required:true
    },
    author:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'User'
    },
    img:{
        type:String
    }
},
{
    timestamps:true
}
);

module.exports = mongoose.model('Feedback', feedbackSchema);