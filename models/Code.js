var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var User = require('./user');
var CodeSchema = new Schema({
    code:{
        type:String,
        required:true
    },
    
    author:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'User'
    }

},
{
    timestamps:true
});

module.exports = mongoose.model('Code', CodeSchema);