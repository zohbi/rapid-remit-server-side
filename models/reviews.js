var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var reviewSchema = new Schema(
  {
    name: {
      type: String,
      required: true,
    },
    text: {
      type: String,
      required: true,
    },
    img: {
      type: String,
    },
    rating: {
      type: Number,
      required: true,
      default: 0,
    },
    partner_id: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "partner",
    },
    user_id: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
    },
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("review", reviewSchema);
