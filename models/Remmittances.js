const mongoose = require('mongoose');
var date = new Date()
const remmittanceScehma= new mongoose.Schema({
    country:{
        type:String,
        required:true
    },
    location:{
        type:String,
        required:true
    },
    amount:{
        type:Number,
        required:true,
    },
    tocountry:{
        type:String,
        required:true,
    },
    recivecurrencyin:{
        type:String,
        required:true,
    },
    sendcurrencyin:{
        type:String,
        required:true,
    },
    reciveamount:{
        type:Number,
        required:true
        
    },
    status:{
        type:String,
    },
    reason:{
        type:String,
    },
    
    author:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'User'
    },
},
{
    timestamps:true
}   
    
    
); 
const Remmittance = mongoose.model('Remmittance',remmittanceScehma);

module.exports = Remmittance;