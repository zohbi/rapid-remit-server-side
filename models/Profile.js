var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var profileSchema = new Schema({
    img:{
        type:String,
        
    },
    author:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'User'
    },

},
{
    timestamps:true
});

module.exports = mongoose.model('Profile', profileSchema);