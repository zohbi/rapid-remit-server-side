var mongoose = require("mongoose");
var Schema = mongoose.Schema;

//var passportLocalMongoose = require("passport-local-mongoose");
var favouriteSchema = new Schema(
  {
    user_id: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User"
    },
    fav_id: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "partner"
    }
  },
  {
    timestamps: true
  }
);

//favouriteSchema.plugin(passportLocalMongoose);

module.exports = mongoose.model("favourite", favouriteSchema);
