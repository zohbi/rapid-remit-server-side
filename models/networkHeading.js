var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var networkHeading = new Schema(
  {
    text: {
      type: String,
      required: true,
    },
  },
  {
    timestamps: true,
  }
);
module.exports = mongoose.model('networkheadings', networkHeading);
