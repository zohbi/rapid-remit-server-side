var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var campaniesSchema = new Schema({
    name:{
        type:String,
    },
    from:{
        type:String,
    },
    to:{
        type:String
    },
    amountWhichWeCharged:{
        type:Number
    },
    sendTo:{
        type:String
    },
    rate:{
        type:Number
    },
    transferDay:{
        type:Number
    },
    fee:{
        type:Number
    },
    sendTo:{
        type:String
    },
    condition:{
        type:String
    },
    transferMethod:{
        type:String
    }

},
{
    timestamps:true
});

module.exports = mongoose.model('companie', campaniesSchema);