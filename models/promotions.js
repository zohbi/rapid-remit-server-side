var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var promotionsSchema = new Schema(
  {
    title: {
      type: String,
      required: true,
    },
    subTitle: {
      type: String,
      required: true,
    },
    text: {
      type: Array,
      required: true,
    },
    expiryDate: {
      type: Date,
      required: true,
    },
    start_date: {
      type: Date,
      required: true,
    },
    avail: {
      type: Boolean,
      default: false,
    },
    img: {
      type: String,
      default: 'No image',
    },
    promotionCode: {
      type: String,
    },
    url: {
      type: String,
    },
    promotionCode2: {
      type: String,
    },
    partner_name: {
      type: String,
      required: true,
    },
    partner_id: {
      type: mongoose.Types.ObjectId,
      ref: 'partner',
      required: true,
    },
    users: [
      {
        type: mongoose.Types.ObjectId,
        ref: 'User',
      },
    ],
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model('Promotion', promotionsSchema);
