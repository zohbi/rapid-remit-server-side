var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var homeBanner = new Schema(
  {
    heading: {
      type: String,
      required: true,
    },
  },
  {
    timestamps: true,
  }
);
module.exports = mongoose.model('homebanners', homeBanner);
