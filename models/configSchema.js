var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var configSchema = new Schema(
  {
    key: {
      type: String,
      required: true,
    },
    title: {
      type: String,
      required: true,
    },
    content: {
      type: String,
      required: true,
    },
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("config", configSchema);
