var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var testimonialSchema = new Schema({
    name:{
        type:String,
        required:true
    },
    text:{
        type:String,
        required:true
    },
    img:{
        type:String,
        required:true
    },
    

},
{
    timestamps:true
});

module.exports = mongoose.model('Testimonial', testimonialSchema);