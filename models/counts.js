var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var count = new Schema(
  {
    rates_count: {
      type: Number,
      default: 0,
      required: true,
    },
  },
  {
    timestamps: true,
  }
);
module.exports = mongoose.model("count", count);
