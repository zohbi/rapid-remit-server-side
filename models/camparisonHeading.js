var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var camparisonHeading = new Schema(
  {
    text: {
      type: String,
      required: true,
    },
  },
  {
    timestamps: true,
  }
);
module.exports = mongoose.model('camparisonheadings', camparisonHeading);
