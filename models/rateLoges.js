var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var rateLogsSchema = new Schema(
    {
        partner: {
            type: String,
            required: true
        },
        base_currency: {
            type: String,
            required: true
        },
        country_name: {
            type: String,
            required: true
        },
        rate: {
            type: Number
        }
    },
    {
        timestamps: true
    }
);

module.exports = mongoose.model("log", rateLogsSchema);
