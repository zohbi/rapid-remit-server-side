var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var faqs = new Schema(
  {
    title: {
      type: String,
      required: true,
    },
    description: {
      type: String,
      required: true,
    },
  },
  {
    timestamps: true,
  }
);
module.exports = mongoose.model('Faq', faqs);
