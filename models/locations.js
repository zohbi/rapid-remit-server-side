var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var locationScheema = new Schema({
  partner_id: {
    type: String,
  },
  location: {
    type: Object,
    coordinates: {
      type: [Number],
      required: true,
      index: '2dsphere',
    },
  },
  address: {
    type: String,
  },
  name: {
    type: String,
  },
});

locationScheema.index({ location: '2dsphere' });

module.exports = mongoose.model('Location', locationScheema);
