var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var uuidv1 = require("uuid/v1");

var passwordHash = require("password-hash");

var superUser = new Schema(
  {
    email: {
      type: String,
      required: true
    },
    hashPassword: {
      type: String,
      required: true
    },
    salt: {
      type: String
    },

    username: {
      type: String,
      required: true
    }
  },
  {
    timestamps: true
  }
);

superUser
  .virtual("password")
  .set(function(password) {
    this._password = password;
    // uuid generate random string
    this.salt = uuidv1();
    this.hashPassword = this.encryptPassword(password);
    //console.log("check", this.salt, this.hashPassword);
  })
  .get(function() {
    return this._password;
  });

superUser.methods = {
  auth: function(password) {
    return passwordHash.verify(password, this.hashPassword);
  },
  encryptPassword: function(password) {
    if (!password) {
      return "";
    }
    try {
      return passwordHash.generate(password);
    } catch (error) {
      console.log(error);
      return "";
    }
  }
};

module.exports = mongoose.model("superUser", superUser);
