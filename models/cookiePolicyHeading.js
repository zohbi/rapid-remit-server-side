var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var cookiePolicyHeading = new Schema(
  {
    text: {
      type: String,
      required: true,
    },
  },
  {
    timestamps: true,
  }
);
module.exports = mongoose.model('cookiepolicyheadings', cookiePolicyHeading);
