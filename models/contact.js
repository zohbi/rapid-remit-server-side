var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var contactSchema = new Schema({
    title:{
        type:String,
        required:true
    },
    name:{
        type:String,
        required:true
    },
    email:{
        type:String,
        required:true
    },
    contact:{
        type:Number,
        required:true
    },
    
},
{
    timestamps:true
}
);

module.exports = mongoose.model('Contact', contactSchema);