var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var privacyPolicyHeading = new Schema(
  {
    text: {
      type: String,
      required: true,
    },
  },
  {
    timestamps: true,
  }
);
module.exports = mongoose.model('privacypolicyheadings', privacyPolicyHeading);
