var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var articleSchema = new Schema(
  {
    bannerTitle: {
      type: String,
      required: true,
    },
    title: {
      type: String,
      required: true,
    },
    text: {
      type: String,
      required: true,
    },
    img: {
      type: String,
      required: true,
    },
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model('Article', articleSchema);
