var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var partnerRatesSchema = new Schema({
    country_name:{
        type:String,
        required:true
    },
    base_currency:{
        type:String,
        required:true
    },
    transfer_time:{
        type:String,
    },
    transfer_method:{
        type:Array,
    },
    payment_methods:{
        type:Array,
    },
    payment_fees:{
        type:Number,
    },
    taxes_rule:{
        type:Number,
    },
    tax_type :{
        type: String
    },
    rules:{
        type:Array,
    },
    partner_name:{
        type:String
    },
    partner_id:{
        type:mongoose.Types.ObjectId,
        ref:"partner"
    },
    limit:{
        type:Number
    }
    
},
{
    timestamps:true
});

module.exports = mongoose.model('partnerrate', partnerRatesSchema);