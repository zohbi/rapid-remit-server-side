var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var termConditionHeading = new Schema(
  {
    text: {
      type: String,
      required: true,
    },
  },
  {
    timestamps: true,
  }
);
module.exports = mongoose.model('termconditionheadings', termConditionHeading);
