var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var passportLocalMongoose = require('passport-local-mongoose');

var partnerSchema = new Schema(
  {
    email: {
      type: String,
      required: true,
    },
    about_exchange: {
      type: String,
    },
    company_type: {
      type: String,
    },
    android_url: {
      type: String,
    },
    ios_url: {
      type: String,
    },
    key_features: {
      type: Array,
    },
    website_url: {
      type: String,
    },
    location_address: {
      type: String,
    },
    map_coordinates: {
      type: Array,
    },
    documents: {
      type: Array,
    },
    payment_transfer_methods: {
      type: Array,
      // enum: ["Bank transfer","Cash payout"]
    },
    payment_methods: {
      type: Array,
      //enum: ["Net Banking","Direct Debit" ,"Wire Transfer"]
    },
    name: {
      type: String,
      required: true,
    },
    phone: {
      type: Number,
    },
    rating: {
      type: Number,
      default: 0,
    },
    img: {
      type: String,
    },
    deletedAt: {
      type: Date,
      default: null,
    },
  },
  {
    timestamps: true,
  }
);

var options = {
  usernameField: 'email',
};
partnerSchema.plugin(passportLocalMongoose, options);
module.exports = mongoose.model('partner', partnerSchema);
