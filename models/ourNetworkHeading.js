var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ourNetworkHeading = new Schema(
  {
    heading: {
      type: String,
      required: true,
    },
    text_1: {
      type: String,
      required: true,
    },
    text_2: {
      type: String,
      required: true,
    },
  },
  {
    timestamps: true,
  }
);
module.exports = mongoose.model('ournetworkheadings', ourNetworkHeading);
