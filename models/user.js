var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var uuidv1 = require('uuid/v1');

var passwordHash = require('password-hash');

var User = new Schema(
  {
    email: {
      type: String,
      required: true,
    },
    hashPassword: {
      type: String,
      required: true,
    },
    salt: {
      type: String,
    },

    admin: {
      type: Boolean,
      default: false,
    },
    firstName: {
      type: String,
      required: true,
    },
    lastName: {
      type: String,
    },
    country: {
      type: String,
    },
    phone: {
      type: Number,
    },
    address: {
      type: String,
    },
    streetAddress: {
      type: String,
    },
    city: {
      type: String,
    },
    role: {
      type: Array,
    },
    code: {
      type: String,
    },
    image: {
      type: String,
      default: null,
    },
    status: {
      type: Boolean,
      required: true,
    },

    forgot_password: {
      status: {
        type: Boolean,
        default: false,
      },
      verification_token: {
        type: String,
        default: null,
      },
      forgot_password_token: {
        type: String,
        default: null,
      },
    },
  },
  {
    timestamps: true,
  }
);

User.virtual('password')
  .set(function (password) {
    this._password = password;
    // uuid generate random string
    this.salt = uuidv1();
    this.hashPassword = this.encryptPassword(password);
    //console.log("check", this.salt, this.hashPassword);
  })
  .get(function () {
    return this._password;
  });

User.methods = {
  auth: function (password) {
    return passwordHash.verify(password, this.hashPassword);
  },
  encryptPassword: function (password) {
    if (!password) {
      return '';
    }
    try {
      return passwordHash.generate(password);
    } catch (error) {
      console.log(error);
      return '';
    }
  },
};

module.exports = mongoose.model('User', User);
