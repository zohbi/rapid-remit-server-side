const express = require('express');
const http = require('http');
const mongoose = require('mongoose');
const app = express();
const DATEOBJ = require('date-and-time');
const Log = require('./models/rateLoges');

var server = require('http').Server(app);

var io = require('socket.io')(server);

app.use(express.json());
const APP = require('./app');
const bodyParser = require('body-parser');
const cors = require('cors');
app.use(cors());
app.use(bodyParser.json());
const axios = require('axios');
const Loges = require('./models/rateLoges');
const Rates = require('./models/rates');
var cron = require('node-cron');

// cron.schedule("0 0 */1 * * *", async () => {
//   console.log("running a task every hour");
//   let rates = await axios.get(
//     "http://exchange.hostedsitedemo.com/exchange.php"
//   );

//   const partners = ["alansariexchange" , "xpressmoney" ,"moneygram"];

//   for(let i=0;i<partners.length;i++){
//     let key = partners[i];
//     const response = await  new Promise(async(resolve,reject)=>{
//       try{
//         await  new Promise(async(resolve,reject)=>{
//             rates.data[key].map(async (rate) => {
//               console.log("TRYING TO PUT VALUE" , rate);
//               const creating = await Loges.create({
//                 partner: key,
//                 base_currency: "AED",
//                 country_name: rate.currency,
//                 rate: rate.rate ? rate.rate : 1,
//               });

//               console.log("Rate Added for now moving to " , creating);

//               const _rate = await Rates.findOne(
//                 { $and: [ { partner: key }, { country_name :rate.currency } ] }
//               );

//               if(_rate){
//                 _rate.rate = rate.rate ? rate.rate : 1;
//                 _rate.base_currency = "AED";
//                 _rate.save();
//               }else{
//                 let _new = {
//                   partner: key,
//                   country_name: rate.currency,
//                   rate: rate.rate ? rate.rate : 1,
//                   base_currency:"AED"
//                 }

//                 let newrate = new Rates(_new);
//                 newrate.save();
//               }

//               resolve(true)
//         });
//       });

//         resolve({success:true});
//       }catch{
//         resolve({success:false});
//       }
//     });

//     console.log("SUCCESS" , response);
//   }

//   // let rates = await axios.get(
//   //   "http://exchange.hostedsitedemo.com/exchange.php"
//   // );
//   //console.log("rates", rates.data);
//   // rates.data["uaeexchange"].map((rate) => {
//   //   //console.log("rayes", rate)
//   //   Loges.create({
//   //     partner: "uaeexchange",
//   //     base_currency: "AED",
//   //     country_name: rate.currency,
//   //     rate: rate.rate ? rate.rate : 1,
//   //   }).then((log) => {
//   //     console.log("loges", log);
//   //   });

//   //   Rates.findOneAndUpdate(
//   //     { partner: "uaeexchange", country_name: rate.currency },
//   //     {
//   //       $set: { rate: rate.rate ? rate.rate : 1 },
//   //     },
//   //     { new: true }
//   //   ).then((rate) => {
//   //     //  console.log("rates", rate)
//   //   });
//   //  });

//   // rates.data["alansariexchange"].map((rate) => {
//   //   //console.log("rayes", rate)

//   //   Loges.create({
//   //     partner: "alansariexchange",
//   //     base_currency: "AED",
//   //     country_name: rate.currency,
//   //     rate: rate.rate ? rate.rate : 1,
//   //   }).then((log) => {
//   //     console.log("loges", log);
//   //   });

//   //   Rates.findOneAndUpdate(
//   //     { partner: "alansariexchange", country_name: rate.currency },
//   //     {
//   //       $set: { rate: rate.rate ? rate.rate : 1 },
//   //     },
//   //     { new: true }
//   //   ).then((rate) => {
//   //     //  console.log("rates", rate)
//   //   });
//   // });

//   // rates.data["xpressmoney"].map((rate) => {
//   //   //console.log("rayes", rate)
//   //   Loges.create({
//   //     partner: "xpressmoney",
//   //     base_currency: "AED",
//   //     country_name: rate.currency,
//   //     rate: rate.rate ? rate.rate : 1,
//   //   }).then((log) => {
//   //     console.log("loges", log);
//   //   });

//   //   Rates.findOneAndUpdate(
//   //     { partner: "xpressmoney", country_name: rate.currency },
//   //     {
//   //       $set: { rate: rate.rate ? rate.rate : 1 },
//   //     },
//   //     { new: true }
//   //   ).then((rate) => {
//   //     //  console.log("rates", rate)
//   //   });
//   // });

//   // rates.data["moneygram"].map((rate) => {
//   //   //console.log("rayes", rate)
//   //   Loges.create({
//   //     partner: "moneygram",
//   //     base_currency: "AED",
//   //     country_name: rate.currency,
//   //     rate: rate.rate ? rate.rate : 1,
//   //   }).then((log) => {
//   //     console.log("loges", log);
//   //   });

//   //   Rates.findOneAndUpdate(
//   //     { partner: "moneygram", country_name: rate.currency },
//   //     {
//   //       $set: { rate: rate.rate ? rate.rate : 1 },
//   //     },
//   //     { new: true }
//   //   ).then((rate) => {
//   //     //  console.log("rates", rate)
//   //   });
//   // });
// });

const chart = async (data) => {
  if (data.country_name && data.time) {
    let date = new Date();
    console.log(
      'date',
      date.getDate(),
      DATEOBJ.addDays(date, -30),
      date.toString()
    );
    //let time = date.getDate() - parseInt(data.time);
    let time = DATEOBJ.addDays(date, -parseInt(data.time));
    let final = `${time.getFullYear()}-${
      time.getMonth() + 1
    }-${time.getDate()}`;
    //let finaDate = new Date(final);
    console.log('final', final);

    let rates = await Log.find({
      partner: data.partners,
      country_name: data.country_name,
      createdAt: { $gte: final },
    });
    console.log('rates', rates);

    let result = {
      [data.country_name]: Array.from(
        new Set(rates.map((r) => r.createdAt.getDate()))
      ).map((d) => {
        return {
          label: d,
          y: rates.find((f) => f.createdAt.getDate() === d).rate,
        };
      }),
    };

    return result;
  }
};
let users = [];
//console.log(users);
console.log('users', users);
io.on('connection', function (socket) {
  console.log('a user connected to client');

  socket.on('log', async (data) => {
    console.log('a user data', socket.id);
    // saving userId to array with socket ID
    //users[socket.id] = data._id;
    if (!users.includes(socket.id)) {
      users = [...users, socket.id];
    }
    console.log('start');
    //users = users + 1;
    console.log('online users', users, users.length);
    socket.broadcast.emit('count', users);
  });

  socket.on('disconnect', async () => {
    console.log('user ' + socket.id + ' disconnected');
    // remove saved socket from users object
    //users = users - 1;
    console.log('online users disconnected before', users);
    users = users.filter(function (item) {
      return item !== socket.id;
    });
    console.log('done');
    console.log('online users disconnected', users, users.length);
    socket.broadcast.emit('count', users);
  });
});

app.use(APP);

server.listen(process.env.PORT || 4000, () => {
  console.log(`Server running `, process.env.PORT || 4000);
});
