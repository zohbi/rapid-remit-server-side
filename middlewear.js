let jwt = require('jsonwebtoken');
const config = require('./config');
const Admin = require('./models/admin');

let checkToken = (req, res, next) => {
  let token = req.headers['x-access-token'] || req.headers['authorization']; // Express headers are auto converted to lowercase

  if (token && token.slice(0, 7) == 'Bearer ') {
    token = token.slice(7, token.length);
  }

  if (token) {
    jwt.verify(token, config.secret, (err, decoded) => {
      if (err) {
        return res.json({
          success: false,
          message: 'Token is not valid',
        });
      } else {
        req.decoded = decoded;
        next();
      }
    });
  } else {
    return res.json({
      success: false,
      message: 'Auth token is not supplied',
    });
  }
};

let adminCheckToken = (req, res, next) => {
  let token = req.headers['x-access-token'] || req.headers['authorization']; // Express headers are auto converted to lowercase

  if (token && token.slice(0, 7) == 'Bearer ') {
    token = token.slice(7, token.length);
  }

  if (token) {
    jwt.verify(token, config.adminSecret, (err, decoded) => {
      if (err) {
        return res.status(400).json({
          status: false,
          statusCode: 400,
          message: err.message,
          data: {},
        });
      } else {
        Admin.findById(decoded.id)
          .then((resp) => {
            if (resp) {
              req.admin = resp;
              next();
            } else {
              return res.status(400).json({
                status: false,
                statusCode: 404,
                message: 'User Not Found',
                data: {},
              });
            }
          })
          .catch((err) => {
            return res.status(400).json({
              status: false,
              statusCode: 400,
              message: err.message,
              data: {},
            });
          });
      }
    });
  } else {
    return res.status(400).json({
      status: false,
      statusCode: 400,
      message: 'Auth token is not supplied',
      data: {},
    });
  }
};

module.exports = {
  checkToken: checkToken,
  adminCheckToken: adminCheckToken,
};
