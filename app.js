var createError = require('http-errors');
var express = require('express');
var path = require('path');
var session = require('express-session');
var FileStore = require('session-file-store')(session);
const usersRouter = require('./routes/users');
const Convert = require('./routes/convert');
const newsRouter = require('./routes/News');
const imageRouter = require('./routes/uploadRouter');
const RemmitanceRouter = require('./routes/Remmittence');
const trackingRouter = require('./routes/tracking');
const adminRouter = require('./routes/admin');
const liveRates = require('./routes/liveRate');
const feedbackRouter = require('./routes/feedbackRoutes');
const specialOffersRouter = require('./routes/specialOffersRoutes');
const promotionsRouter = require('./routes/promotionsRoutes');
const contactUsRouter = require('./routes/contactUsRoutes');
const TestRouter = require('./routes/TestimonialRoutes');
const articleRouter = require('./routes/articlesRoutes');
const blogRouter = require('./routes/blogRoutes');
const offersRouter = require('./routes/offersRoutes');
const settingRouter = require('./routes/settingRoutes');
const faqsRouter = require('./routes/faqsRoutes');
const homeBannerRouter = require('./routes/homeBannerRoutes');
const subHeadingsRouter = require('./routes/subHeadingsRoutes');
const networkHeadingsRouter = require('./routes/networkHeadingsRoutes');
const servicesHeadingsRouter = require('./routes/servicesHeadingsRoutes');

const camparisonHeadingsRouter = require('./routes/camparisonHeadingsRoutes');
const ourNetworkHeadingsRouter = require('./routes/ourNetworkHeadingsRoutes');
const aboutUsHeadingsRoutes = require('./routes/aboutUsHeadingsRoutes');
const doHeadingsRoutes = require('./routes/doHeadingsRoutes');
const MVHeadingsRoutes = require('./routes/MVHeadingsRoutes');
const cookiePolicyHeadingsRoutes = require('./routes/cookiePolicyHeadingsRoutes');
const privacyPolicyHeadingsRoutes = require('./routes/privacyPolicyHeadingsRoutes');
const termConditionHeadingsRoutes = require('./routes/termConditionHeadingsRoutes');

//const csvFile = require("./routes/csvFile");
var passport = require('passport');
const partnerRouter = require('./routes/partnerRoutes');
const partnerRateRouter = require('./routes/partnerRatesRouter');
const reviewRouter = require('./routes/reviewRoutes');
const rateRouter = require('./routes/ratesRoutes');
const favRouter = require('./routes/favouriteRoutes');
const superUser = require('./routes/superUser');
const content = require('./routes/content');
const count = require('./routes/countRoutes');
const footerRoutes = require('./routes/footerRoutes');
const locationRoutes = require('./routes/locations');
const currenciesRoutes = require('./routes/currencies');
const currenciesRatesRoutes = require('./routes/currenciesRates');

const config = require('./config');
const mongoose = require('mongoose');
const uri = config.mongoURL;

//const url = 'mongodb://localhost:27017/RapidRemit';
//const uri = process.env.MONGODB_URI || 'mongodb://localhost/rapidRemit';
const connect = mongoose.connect(uri, {
  useNewUrlParser: true,
  useCreateIndex: true,
  useFindAndModify: false,
  useUnifiedTopology: true,
});

connect.then(
  (db) => {
    console.log('Connected correctly to server');
  },
  (err) => {
    console.log(err);
  }
);
var app = express();
//app.use(cookieParser('12345-67890-09876-54321'));
app.use(express.static('public'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
const cors = require('cors');
app.use(cors());
//app.use(cookieParser('12345-67890-09876-54321'));

// app.use(
//   session({
//     name: "session-id",
//     secret: "12345-67890-09876-54321",
//     saveUninitialized: false,
//     resave: false,
//     store: new FileStore()
//   })
// );

app.use(passport.initialize());
//app.use(passport.session());
app.use('/partner', partnerRouter);
app.use('/partnerrate', partnerRateRouter);
app.use('/favourite', favRouter);
app.use('/review', reviewRouter);
app.use('/users', usersRouter);
app.use('/contactUs', contactUsRouter);
app.use('/testimonial', TestRouter);
app.use('/offers', specialOffersRouter);
app.use('/liveRates', liveRates);
app.use('/feedback', feedbackRouter);
app.use('/blog', blogRouter);
app.use('/settings', settingRouter);
app.use('/offers', offersRouter);
app.use('/faqs', faqsRouter);
app.use('/home-banners', homeBannerRouter);
app.use('/sub-headings', subHeadingsRouter);
app.use('/network-headings', networkHeadingsRouter);
app.use('/services-headings', servicesHeadingsRouter);
app.use('/camparison-headings', camparisonHeadingsRouter);
app.use('/our-network-headings', ourNetworkHeadingsRouter);
app.use('/about-us-headings', aboutUsHeadingsRoutes);
app.use('/what-we-do-headings', doHeadingsRoutes);
app.use('/mission-vision-headings', MVHeadingsRoutes);
app.use('/cookie-policy-headings', cookiePolicyHeadingsRoutes);
app.use('/privacy-policy-headings', privacyPolicyHeadingsRoutes);
app.use('/term-policy-headings', termConditionHeadingsRoutes);
app.use('/article', articleRouter);
app.use('/convert', Convert);
app.use('/rate', rateRouter);
app.use('/superUser', superUser);
app.use('/promotions', promotionsRouter);
app.use('/content', content);
app.use('/count', count);
app.use('/footer', footerRoutes);
app.use('/locations', locationRoutes);
app.use('/currencies', currenciesRoutes);
app.use('/currencies-rates', currenciesRatesRoutes);

//app.use('/csv',csvFile);

function auth(req, res, next) {
  if (!req.user) {
    var err = new Error('You are not authenticated!');
    err.status = 403;
    next(err);
  } else {
    next();
  }
}
//app.use(auth);
app.use('/uploadImage', imageRouter);
app.use('/tracking', trackingRouter);
app.use('/remmitance', RemmitanceRouter);

app.use('/news', newsRouter);

function verifyAdmin(req, res, next) {
  if (req.user && !req.user.admin) {
    console.log(req.user);
    var err = new Error('You are not admin!');
    err.status = 403;
    next(err);
  } else {
    next();
  }
}
app.use(verifyAdmin);
app.use('/admin', adminRouter);

module.exports = app;
