var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var JwtStrategy = require('passport-jwt').Strategy;
var ExtractJwt = require('passport-jwt').ExtractJwt;
var jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens
var config = {'secretKey': '12345-67890-09876-54321'};
var Partner = require('./models/partners');
passport.use(new LocalStrategy({
    usernameField: 'email'
}, Partner.authenticate()));
passport.serializeUser(Partner.serializeUser());
passport.deserializeUser(Partner.deserializeUser());

exports.getToken = function (user) {
    return jwt.sign(user, config.secretKey,
        { expiresIn: 36000 });
};

var opts = {};
opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
opts.secretOrKey = config.secretKey;

exports.jwtPassport = passport.use(new JwtStrategy(opts,
    (jwt_payload, done) => {
        console.log("JWT payload: ", jwt_payload);
        Partner.findOne({ _id: jwt_payload._id }, (err, user) => {
            if (err) {
                return done(err, false);
            }
            else if (user) {
                return done(null, user);
            }
            else {
                return done(null, false);
            }
        });
    }));

exports.verifyUser = passport.authenticate('jwt', { session: false });

// exports.verifyOrdinaryUser = function (req, res, next) {
//     var t = req.headers['authorization'];
//     var token = t.slice(7);
//     console.log("rkn", token)
//     if (token) {
//         jwt.verify(token, config.secretKey, function (err, decoded) {
//             if (err) {
//                 var err = new Error('You are not authenticated!');
//                 err.status = 401;
//                 return next(err);
//             } else {
//                 req.decoded = decoded;
//                 console.log(req.decoded)
//                 next();
//             }
//         });
//     } else {
//         var err = new Error('No token provided!');
//         err.status = 403;
//         return next(err);
//     }
// };

// exports.verifyAdmin = function (req, res, next) {
//     console.log("decode", req.decoded._id)
//     User.findById(req.decoded._id)
//         .then(resp => {
//             if (resp.admin) {
//                 //console.log("resp" , resp);
//                 next();
//             }
//             else {
//                 console.log("You are not admin!")
//                 var err = new Error('You are not authenticated!');
//                 err.status = 403;
//                 next(err);

//             }
//         })


// };