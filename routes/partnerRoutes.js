const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const Promotions = require('../models/promotions');
const Partner = require('../models/partners');
const boom = require('boom');
const Joi = require('@hapi/joi');
let passport = require('passport');
var authenticate = require('../authenticate_partner');
const router = express.Router();
const axios = require('axios');
const PartnerRates = require('../models/partnerrates');
const Reviews = require('../models/reviews');

router.get('/', async (req, res, next) => {
  console.log('done');

  const lookup = await Partner.aggregate([
    {
      $lookup: {
        from: 'reviews',
        localField: '_id',
        foreignField: 'partner_id',
        as: 'reviews',
      },
    },
    { $project: { salt: 0, hash: 0 } },
  ]);

  res.statusCode = 200;
  res.setHeader('Content-Type', 'application/json');

  res.json(lookup);
});

router.get('/:id', (req, res, next) => {
  console.log('done');
  if (req.params.id && req.params.id.length == 24) {
    Partner.findById(req.params.id)
      .then(
        (resp) => {
          res.statusCode = 200;
          res.setHeader('Content-Type', 'application/json');
          res.json(resp);
        },
        (err) => next(err)
      )
      .catch((err) => next(err));
  } else {
    res.statusCode = 400;
    res.setHeader('Content-Type', 'application/json');
    res.json('Invalid partner Id ');
  }
});

router.post('/', (req, res, next) => {
  const data = req.body;
  let array = [];
  const schema = Joi.object().keys({
    email: Joi.string().email().required(),
    about_exchange: Joi.string().min(3),
    company_type: Joi.string(),
    android_url: Joi.string(),
    ios_url: Joi.string(),
    key_features: Joi.array(),
    website_url: Joi.string(),
    location_address: Joi.string(),
    map_coordinates: Joi.array(),
    documents: Joi.array(),
    payment_methods: Joi.array(),
    payment_transfer_methods: Joi.array(),
    username: Joi.string().required(),
    password: Joi.string().required(),
    img: Joi.string(),
    phone: Joi.number(),
  });
  const val = schema.validate(data);
  if (!val.error) {
    // img:req.body.img , email: data.email, name: data.name, about_exchange: data.about_exchange, company_type: data.company_type, android_url: data.android_url, ios_url: data.ios_url, key_features: data.key_features, website_url: data.website_url, location_address: data.location_address, map_coordinates: data.map_coordinates, documents: data.documents, payment_methods: data.payment_methods, payment_transfer_methods: data.payment_transfer_methods
    var done = false;
    Partner.register(
      new Partner({ email: data.email, name: data.username, rating: 5 }),
      data.password,
      async (err, user) => {
        //console.log("user", user);
        if (err) {
          res.statusCode = 500;
          res.setHeader('Content-Type', 'application/json');
          res.json({ err: err });
        } else {
          passport.authenticate('local');
          // let name = data.username.split(" ").join("").toLowerCase();
          // if (
          //   name == "uaeexchange" ||
          //   name == "xpressmoney" ||
          //   name == "moneygram" ||
          //   name == "alansariexchange"
          // ) {
          //   let rates = await axios.get(
          //     "http://exchange.hostedsitedemo.com/exchange.php"
          //   );
          //   console.log("check", rates.data[name]);
          //   rates.data[name].map(async (rate) => {
          //     console.log("dekha", rate);
          //     let partenerRates = await PartnerRates.create({
          //       base_currency: "AED",
          //       country_name: rate.currency,
          //       partner_name: user.name,
          //       partner_id: user._id,
          //     });
          //     //.then(resp => {
          //     console.log("created", partenerRates);
          //     array.push(partenerRates);
          //     // });
          //     console.log("array", array);
          //   });
          // }
          res.statusCode = 200;
          res.setHeader('Content-Type', 'application/json');
          res.json(user);
          //res.json({ success: true, status: 'Registration Successful!', data: user });
        }
      }
    );
    //res.redirect(307, "/partner/login");
  } else {
    res.statusCode = 400;
    res.setHeader('Content-Type', 'application/json');
    res.json(val.error);
  }
});

router.post('/login', passport.authenticate('local'), (req, res, err) => {
  var token;
  req.user
    ? ((token = authenticate.getToken({ _id: req.user._id })),
      (res.statusCode = 200),
      res.setHeader('Content-Type', 'application/json'),
      res.json({
        success: true,
        token: token,
        status: 'you are succesfully loged in',
        data: req.user,
      }))
    : null;
});

router.put('/:id', authenticate.verifyUser, (req, res, next) => {
  const data = req.body;
  console.log('done');
  if (req.params.id && req.params.id.length == 24) {
    const schema = Joi.object().keys({
      about_exchange: Joi.string().min(3),
      company_type: Joi.string(),
      android_url: Joi.string(),
      ios_url: Joi.string(),
      key_features: Joi.array(),
      website_url: Joi.string(),
      location_address: Joi.string(),
      map_coordinates: Joi.array(),
      documents: Joi.array(),
      payment_methods: Joi.array(),
      payment_transfer_methods: Joi.array(),
      name: Joi.string().required(),
      phone: Joi.number(),
      img: Joi.string(),
      //password: Joi.string().required(),
      //_id: Joi.string()
    });
    const val = schema.validate(data);
    if (!val.error) {
      Partner.findByIdAndUpdate(
        req.params.id,
        {
          $set: data,
        },
        { new: true }
      ).then((resp) => {
        (res.statusCode = 200),
          res.setHeader('Content-Type', 'application/json'),
          res.json(resp);
      });
    } else {
      res.statusCode = 400;
      res.setHeader('Content-Type', 'application/json');
      res.json(val.error);
    }
  } else {
    res.statusCode = 400;
    res.setHeader('Content-Type', 'application/json');
    res.json('Invalid partner Id ');
  }
});

router.delete('/:id', authenticate.verifyUser, (req, res, next) => {
  console.log('done');
  if (req.params.id && req.params.id.length == 24) {
    Partner.findByIdAndRemove(req.params.id).then((resp) => {
      (res.statusCode = 200),
        res.setHeader('Content-Type', 'application/json'),
        res.json(resp);
    });
  } else {
    res.statusCode = 400;
    res.setHeader('Content-Type', 'application/json');
    res.json('Invalid partner Id ');
  }
});

router.post('/favourite/:id', (req, res, err) => {
  let logedin_user = req.user;
  console.log(logedin_user);
});

module.exports = router;
