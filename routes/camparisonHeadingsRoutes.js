const express = require('express');
const CamparisonHeadings = require('../models/camparisonHeading');
const router = express.Router();

router.route('/').get((req, res, next) => {
  CamparisonHeadings.findOne({})
    .then(
      (resp) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(resp);
      },
      (err) => next(err)
    )
    .catch((err) => next(err));
});

module.exports = router;
