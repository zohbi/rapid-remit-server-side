const express = require('express');
const TermConditionHeadings = require('../models/termConditionHeading');
const router = express.Router();

router.route('/').get((req, res, next) => {
  TermConditionHeadings.findOne({})
    .then(
      (resp) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(resp);
      },
      (err) => next(err)
    )
    .catch((err) => next(err));
});

module.exports = router;
