var express = require('express');
const bodyParser = require('body-parser');
var User = require('../models/user');
const middlewear = require('../middlewear');

const mongoose = require('mongoose');
//var app = express();
var passport = require('passport');
var nodemailer = require('nodemailer');
var fs = require('fs');
var router = express.Router();
//console.log('users connectes');
router.use(bodyParser.json());
let jwt = require('jsonwebtoken');
//router.use(express.json());
var mkdirp = require('mkdirp');
const config = require('../config');
const crypto = require('crypto');
const sgMail = require('@sendgrid/mail');
sgMail.setApiKey(
  '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'
);

router.post('/signup', async (req, res) => {
  console.log('start');
  if (req.body.password.length < 8) {
    console.log(req.body.password.length);
    res.statusCode = 500;
    res.json({ err: 'password must have atleast 8 chracters' });
    console.log('password must have atleast 8 chracters');
  }
  if (req.body.password != req.body.conformPassword) {
    res.setHeader('Content-Type', 'application/json');
    res.statusCode = 500;
    res.json({ err: 'password does not matched' });
    console.log('password does not matched');
  } else {
    if (!req.body.admin) {
      req.body.admin = false;
    }
    const { email } = req.body;
    const existUser = await User.findOne({ email });

    let _a;
    if (!existUser) {
      _a = new User({
        email: req.body.email,
        streetAddress: req.body.streetAddress,
        address: req.body.address,
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        country: req.body.country,
        city: req.body.city,
        phone: req.body.phone,
        role: req.body.role,
        admin: req.body.admin,
        password: req.body.password,
        status: true,
      });

      let user = await _a.save();
      let token = jwt.sign({ email: req.body.email }, config.secret, {
        expiresIn: '24h', // expires in 24 hours
      });

      if (user.err) {
        res.code(400).send({ val: _a.err, error: true });
        //return { val: _a.err, error: true };
      } else {
        //ssport.authenticate("local");
        // if (auth) {

        user.token = await token;
        // user.hashPassword = undefined;
        // user.salt = undefined;
        res.setHeader('Content-Type', 'application/json');
        res.statusCode = 200;
        res.json({
          success: true,
          status: 'Registration Successful!',
          data: user,
          token: token,
        });
      }
      // res.json({
      //   success: true,
      //   status: "Registration Successful!",
      //   data: user,
      //   token: token
      // });
    } else {
      res.setHeader('Content-Type', 'application/json');
      res.statusCode = 400;
      res.json({
        success: false,
        status: 'Email already exsists',
      });
    }
  }
});

router.get('/', async (req, res) => {
  let users = await User.find({}, { hash: 0, salt: 0, hashPassword: 0 });
  console.log('Users', users);
  res.setHeader('Content-Type', 'application/json');
  res.statusCode = 200;
  res.json(users);
});

router.put('/changeStatus/:id', middlewear.checkToken, async (req, res) => {
  let id = req.params.id;
  let users = await User.findById(id);
  let status = !users.status;
  let update = await User.findByIdAndUpdate(
    id,
    {
      $set: { status: status },
    },
    { new: true }
  );

  console.log('Users', users, update);
  res.setHeader('Content-Type', 'application/json');
  res.statusCode = 200;
  res.json({ status: update.status });
});

router.put('/:id', async (req, res) => {
  let id = req.params.id;
  let update = await User.findByIdAndUpdate(
    id,
    {
      $set: req.body,
    },
    { new: true }
  );

  res.setHeader('Content-Type', 'application/json');
  res.statusCode = 200;
  res.json(update);
});

// router.post("/signup", (req, res, next) => {
//   if (req.body.password.length < 8) {
//     console.log(req.body.password.length);
//     res.statusCode = 500;
//     res.json({ err: "password must have atleast 8 chracters" });
//     console.log("password must have atleast 8 chracters");
//   }
//   if (req.body.password != req.body.conformPassword) {
//     res.setHeader("Content-Type", "application/json");
//     res.statusCode = 500;
//     res.json({ err: "password does not matched" });
//     console.log("password does not matched");
//   } else {
//     if (!req.body.admin) {
//       req.body.admin = false;
//     }
//     User.register(
//       new User({
//         email: req.body.email,
//         streetAddress: req.body.streetAddress,
//         address: req.body.address,
//         firstName: req.body.firstName,
//         lastName: req.body.lastName,
//         country: req.body.country,
//         city: req.body.city,
//         phone: req.body.phone,
//         role: req.body.role,
//         admin: req.body.admin
//       }),
//       req.body.password,
//       (err, user) => {
//         console.log("Data", user);
//         console.log("err", err);

//         if (err) {
//           res.statusCode = 500;
//           res.setHeader("Content-Type", "application/json");
//           res.json({ err: err });
//         } else {
//           passport.authenticate("local");
//           res.statusCode = 200;
//           res.setHeader("Content-Type", "application/json");

//           mkdirp("./public/" + user.email, function(err) {
//             if (err) console.error(err);
//             else console.log("pow!");
//           });

//           res.json({
//             success: true,
//             status: "Registration Successful!",
//             data: user
//           });
//         }
//       }
//     );
//   }
// });
router.post('/login', (req, res, err) => {
  let data = req.body;
  User.findOne({ email: data.email }).then((user) => {
    console.log('user', user);
    if (!user) {
      return res.status(404).json({ error: 'User not found' });
    }
    if (!user.auth(data.password)) {
      console.log('---', user);
      return res.status(401).json({ error: 'Password incorrect' });
    }
    if (!user.status) {
      console.log('---', user);
      return res
        .status(403)
        .json({ error: 'your account is blocked by Admin' });
    }
    //var token = auth.getToken({ _id: user._id });

    let token = jwt.sign({ email: data.email }, config.secret, {
      expiresIn: '24h', // expires in 24 hours
    });
    // return the JWT token for the future API calls
    // res.json({
    //   success: true,
    //   message: 'Authentication successful!',
    //   token: token
    // });

    // user.hashPassword = undefined;
    // user.salt = undefined;

    res.setHeader('Content-Type', 'application/json');
    res.statusCode = 200;
    res.json({
      success: true,
      token: token,
      status: 'you are succesfully loged in',
      data: user,
    });
  });
});

router.get('/logout', (req, res, next) => {
  console.log('logout');
  if (req.session) {
    req.session.destroy();
    res.clearCookie('session-id');
    res.json('Loged out');
  } else {
    var err = new Error('You are not logged in!');
    err.status = 403;
    next(err);
  }
});

// router.post('/forgot', (req, res) => {
//   function randomInt(low, high) {
//     return Math.floor(Math.random() * (high - low) + low);
//   }
//   var code = randomInt(1000, 9999);
//   console.log(code);

//   User.findOne({ email: req.body.email }).then((u) => {
//     let id = u._id;
//     console.log(id);
//     User.findById(id, function (err, doc) {
//       if (err) console.log(err);
//       doc.code = code;
//       doc.save((code, err) => {
//         if (err) console.log(err);
//         console.log(code);
//       });
//     });
//   });
//   var transporter = nodemailer.createTransport({
//     service: 'Gmail',
//     auth: {
//       user: 'muhamadhasan043@gmail.com',
//       pass: 'artificial.intelligence',
//     },
//   });

//   var mailOptions = {
//     from: 'muhamadhasan043@gmail.com',
//     to: req.body.email,
//     subject: 'password Reset of Rapid Remmit',
//     text:
//       'your verification code is ' +
//       code +
//       ' .\nGo and paste in your app.\nThanks',
//   };

//   transporter.sendMail(mailOptions, function (error, info) {
//     if (error) {
//       console.log(error);
//     } else {
//       console.log('Email sent: ' + info.response);

//       res.json({ status: 'Email has successfully sended' });
//     }
//   });
// });

router.post('/forgot', async (req, res) => {
  try {
    const email = req.body.email;
    const user = await User.findOne({ email: email });

    if (!user) {
      res.setHeader('Content-Type', 'application/json');
      res.statusCode = 404;
      res.json('User doesn’t exist');
    } else {
      const token = crypto.randomBytes(20).toString('hex');

      const mailOptions = {
        to: user.email,
        from: 'rapidremit1@gmail.com',
        subject: 'Password change request',
        html: `Hi ${user.firstName} ${user.lastName} <br> 
      Please click on the following link : <br> <br> <br> <a style="box-shadow: 0px 0px 0px 2px #9fb4f2;
      background:linear-gradient(to bottom, #7892c2 5%, #476e9e 100%);
      background-color:#7892c2;
      border-radius:10px;
      border:1px solid #4e6096;
      display:inline-block;
      cursor:pointer;
      color:#ffffff;
      font-family:Arial;
      font-size:19px;
      padding:12px 37px;
      text-decoration:none;
      text-shadow:0px 1px 0px #283966;" align="center" class="example_b" href='https://rapidremit.biz/forgetpassword/${token}' target="_blank" >Link</a> 
       <br> <br> <br> to reset your password. <br><br> 
      If you did not request this, please ignore this email and your password will remain unchanged.\n`,
      };
      sgMail
        .send(mailOptions)
        .then(() => {
          console.log('Email sent');
        })
        .catch((error) => {
          console.error(error.response.body);
        });

      const updateUser = await User.findOneAndUpdate(
        { email: email },
        {
          'forgot_password.status': true,
          'forgot_password.verification_token': token,
        }
      );

      res.setHeader('Content-Type', 'application/json');
      res.statusCode = 200;
      res.json({ message: 'successful' });
    }
  } catch (err) {
    console.log(err);
  }
});

router.post('/recover/:token', (req, res) => {
  User.findOne({
    'forgot_password.status': true,
    'forgot_password.verification_token': req.params.token,
  })
    .then((user) => {
      if (!user) {
        return res
          .status(401)
          .json({ err: 'Password reset token is invalid or has expired.' });
      }

      User.findOneAndUpdate(
        { _id: user._id },
        {
          'forgot_password.status': false,
          'forgot_password.verification_token': '',
        }
      ).then((user) => {});
      res.setHeader('Content-Type', 'application/json');
      res.statusCode = 200;
      res.json({ user: user, message: 'successful' });
    })
    .catch((err) => res.status(500).json({ message: err.message }));
});

router.post('/changepassword', function (req, res) {
  User.findOne({ email: req.body.email }).then((data) => {
    if (data.code == req.body.code) {
      console.log('right');
      var newpassword = req.body.newpassword;
      if (newpassword.length < 8) {
        console.log(newpassword.length);
        res.json({ err: 'password must have atleast 8 chracters' });
        console.log('password must have atleast 8 chracters');
      } else {
        User.findOne({ email: req.body.email }).then(function (sanitizedUser) {
          if (sanitizedUser) {
            console.log(sanitizedUser);
            sanitizedUser.password = req.body.newpassword;
            sanitizedUser.save();
            res.json({ message: 'password reset successful' });
          }
        });
      }
    } else {
      res.json({ err: 'please enter a correct code' });
    }
  });
});

module.exports = router;
