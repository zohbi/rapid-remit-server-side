router.post('/chart', async (req, res, next) => {
  const data = req.body;
  console.log('asd');
  if (data.country_name && data.time) {
    let date = new Date();
    console.log(
      'date',
      date.getDate(),
      DATEOBJ.addDays(date, -30),
      date.toString()
    );
    //let time = date.getDate() - parseInt(data.time);
    let time = DATEOBJ.addDays(date, -parseInt(data.time));
    let final = `${time.getFullYear()}-${
      time.getMonth() + 1
    }-${time.getDate()}`;
    //let finaDate = new Date(final);
    console.log('final', final);
    let countries = [
      'AUD',
      'CAD',
      'EGP',
      'EUR',
      'GBP',
      'INR',
      'JOD',
      'LKR',
      'NPR',
      'PHP',
      'PKR',
      'USD',
    ];
    let allRates = [];
    let result = [];
    for (let i = 0; i < countries.length; i++) {
      let rates = await Log.find({
        partner: data.partners,
        country_name: countries[i],
        createdAt: { $gte: final },
      });
      //console.log("rates", rates);
      //allRates[i] = rates;
      //console.log("allRated", allRates[i]);

      result[countries[i]] = Array.from(
        new Set(rates.map((r) => r.createdAt.getDate()))
      ).map((d) => {
        return {
          label: d,
          y: rates.find((f) => f.createdAt.getDate() === d).rate,
        };
      });
    }
    console.log('rates', result);
    let comp = await result;

    // let result = Array.from(
    //   new Set(rates.map((r) => r.createdAt.getDate()))
    // ).map((d) => {
    //   return {
    //     label: d,
    //     y: rates.find((f) => f.createdAt.getDate() === d).rate,
    //   };
    // });

    // rate = [...new Set(rates.map((x) => x.createdAt))];
    // console.log("count", rate);
    res.statusCode = 200;
    res.setHeader('Content-Type', 'application/json');
    res.json(comp);
  }
});
