const express = require('express');

const Contact = require('../models/contact');

const router = express.Router();

router.route('/').post((req, res, next) => {
  Contact.create(req.body)
    .then(
      (resp) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(resp);
      },
      (err) => next(err)
    )
    .catch((err) => next(err));
});

module.exports = router;
