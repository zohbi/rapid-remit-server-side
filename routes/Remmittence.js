const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");

const Remmittance = require("../models/Remmittances");
var converter = require("../api");
//const accountRouter = express.Router();
const axios = require("axios");

const router = express.Router();

router
  .route("/:id")
  .get((req, res, next) => {
    var u = req.params.id;
    Remmittance.find({ author: u })
      .then(
        remmit => {
          res.statusCode = 200;
          res.setHeader("Content-Type", "application/json");
          res.json(remmit);
        },
        err => next(err)
      )
      .catch(err => next(err));
  })
  .post((req, res, next) => {
    //req.body.author =req.user._id;
    var p;
    req.body.reciveamount = converter.currency(
      req.body.sendcurrencyin,
      req.body.recivecurrencyin,
      req.body.amount,
      New => {
        console.log("amoun", New);
        req.body.reciveamount = New;
        Remmittance.create(req.body)
          .then(
            remmit => {
              res.statusCode = 200;
              res.setHeader("Content-Type", "application/json");
              res.json(remmit);
            },
            err => next(err)
          )
          .catch(err => next(err));
      }
    );
  })
  .delete((req, res, next) => {
    var u = req.body._id;
    Remmittance.remove({ author: u })
      .then(
        resp => {
          res.statusCode = 200;
          res.setHeader("Content-Type", "application/json");
          res.json(resp);
        },
        err => next(err)
      )
      .catch(err => next(err));
  });

router
  .route("/:Id")
  .get((req, res, next) => {
    Remmittance.findById(req.params.Id)
      .then(
        remp => {
          res.statusCode = 200;
          res.setHeader("Content-Type", "application/json");
          res.json(remp);
        },
        err => next(err)
      )
      .catch(err => next(err));
  })
  .delete((req, res, next) => {
    Remmittance.findByIdAndRemove(req.params.Id)
      .then(
        resp => {
          res.statusCode = 200;
          res.setHeader("Content-Type", "application/json");
          res.json(resp);
        },
        err => next(err)
      )
      .catch(err => next(err));
  });

router.route("/last/id").get((req, res, next) => {
  var u = req.user._id;
  Remmittance.findOne({ author: u })
    .sort({ _id: -1 })
    .then(
      remmit => {
        res.statusCode = 200;
        res.setHeader("Content-Type", "application/json");
        res.json(remmit);
      },
      err => next(err)
    )
    .catch(err => next(err));
});

module.exports = router;
