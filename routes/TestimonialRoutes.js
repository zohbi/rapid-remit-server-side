const express = require("express");
const bodyParser = require("body-parser");
const User = require("../models/user");
const Test = require("../models/testimonial");
const router = express.Router();
const middlewear = require("../middlewear");
router.use(bodyParser.json());

router.post("/", middlewear.checkToken, async (req, res) => {
  const data = req.body;
  let test = new Test(data);
  test.save();
  res.setHeader("Content-Type", "application/json");
  res.statusCode = 200;
  res.json(test);
});

router.put("/:id", middlewear.checkToken, async (req, res) => {
  const data = req.body;
  let test = await Test.findByIdAndUpdate(
    req.params.id,
    { $set: data },
    { new: true }
  );
  res.setHeader("Content-Type", "application/json");
  res.statusCode = 200;
  res.json(test);
});

router.delete("/:id", middlewear.checkToken, async (req, res) => {
  const data = req.body;
  let test = await Test.findByIdAndRemove(req.params.id);
  res.setHeader("Content-Type", "application/json");
  res.statusCode = 200;
  res.json(test);
});

router.get("/", async (req, res) => {
  let test = await Test.find();

  res.setHeader("Content-Type", "application/json");
  res.statusCode = 200;
  res.json(test);
});

router.get("/:name", (req, res, next) => {
  Test.findOne({ name: req.params.name }).then((resp) => {
    //console.log(i.img);
    res.setHeader("Content-Type", "application/json");
    res.json(resp);
  });
});

module.exports = router;
