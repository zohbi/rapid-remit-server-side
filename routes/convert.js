const axios = require("axios");
const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");

const API = require("../api");

const router = express.Router();

router.use(bodyParser.json());

router.route("/").post((req, res, next) => {
  API.currency(req.body.to, req.body.from, req.body.amount, (value) => {
    var rate = value / req.body.amount;
    console.log(rate);
    console.log("value", value);
    res.statusCode = 200;
    res.setHeader("Content-Type", "application/json");
    res.json({ value: value, rate: rate });
  });
});

module.exports = router;
