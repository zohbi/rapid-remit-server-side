const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const PartnerRates = require('../models/partnerrates');
const PartnerLocation = require('../models/locations');

const boom = require('boom');
const Joi = require('@hapi/joi');
const Partner = require('../models/partners');
const authenticate = require('../authenticate_partner');
const router = express.Router();
router.use(bodyParser.json());
const Rates = require('../models/rates');
const Count = require('../models/counts');

const multer = require('multer');

router.get('/:id', authenticate.verifyUser, async (req, res, next) => {
  const id = req.params.id;
  if (id) {
    const _locations = await PartnerLocation.find({ partner_id: id });
    res.statusCode = 200;
    res.setHeader('Content-Type', 'application/json');
    res.json({ success: true, locations: _locations });
  } else {
    res.statusCode = 400;
    res.setHeader('Content-Type', 'application/json');
    res.json({ success: false });
  }
});

router.post('/add/:uid', authenticate.verifyUser, async (req, res, next) => {
  const id = req.params.uid;

  if (id) {
    // const _locations = await PartnerLocation.find({partner_id:id})

    const loc = {
      partner_id: id,
      location: {
        type: 'Point',
        coordinates: [+req.body.longitude, +req.body.latitude],
      },
      address: req.body.address,
      name: req.body.title,
    };

    const _location = new PartnerLocation(loc);

    await _location.save();
    console.log(_location);
    res.statusCode = 200;
    res.setHeader('Content-Type', 'application/json');
    res.json({ success: true, data: _location });
  } else {
    res.statusCode = 400;
    res.setHeader('Content-Type', 'application/json');
    res.json({ success: false });
  }
});

router.put(
  '/update/:location_id/:partner_id',
  authenticate.verifyUser,
  async (req, res, next) => {
    const partner_id = req.params.partner_id;
    const location_id = req.params.location_id;

    if (location_id) {
      // const _locations = await PartnerLocation.find({partner_id:id})

      const loc = {
        partner_id: partner_id,
        location: {
          type: 'Point',
          coordinates: [req.body.longitude, req.body.latitude],
        },
        address: req.body.address,
        name: req.body.title,
      };

      const _location = await PartnerLocation.findByIdAndUpdate(location_id, {
        ...loc,
      });

      console.log(_location);
      res.statusCode = 200;
      res.setHeader('Content-Type', 'application/json');
      res.json({ success: true, data: _location });
    } else {
      res.statusCode = 400;
      res.setHeader('Content-Type', 'application/json');
      res.json({ success: false });
    }
  }
);

router.delete(
  '/:location_id',
  authenticate.verifyUser,
  async (req, res, next) => {
    const location_id = req.params.location_id;
    if (location_id) {
      const _location = await PartnerLocation.findByIdAndDelete(
        req.params.location_id
      );

      console.log(_location);

      console.log(_location);
      res.statusCode = 200;
      res.setHeader('Content-Type', 'application/json');
      res.json({ success: true, data: _location });
    } else {
      res.statusCode = 400;
      res.setHeader('Content-Type', 'application/json');
      res.json({ success: false });
    }
  }
);
module.exports = router;
