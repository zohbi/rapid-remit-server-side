const express = require('express');
const router = express.Router();
const auth = require('./admin/auth');
const news = require('./admin/news');
const testimonials = require('./admin/testimonals');
const blogs = require('./admin/blogs');
const articles = require('./admin/articles');
const offers = require('./admin/offers');
const faqs = require('./admin/faqs');
const settings = require('./admin/settings');
const homeBanners = require('./admin/homeBanners');
const subHeadings = require('./admin/subHeadings');
const servicesHeadings = require('./admin/servicesHeadings');
const networkHeadings = require('./admin/networkHeadings');

const camparisonHeadings = require('./admin/camparisonHeadings');
const ourNetworkHeadings = require('./admin/ourNetworkHeadings');
const aboutUsHeadings = require('./admin/aboutUsHeadings');
const doHeadings = require('./admin/doHeadings');
const MVHeadings = require('./admin/mvHeadings');
const cookiePolicyHeadings = require('./admin/cookiePolicyHeadings');
const privacyPolicyHeadings = require('./admin/privacyPolicyHeadings');
const termConditionHeadings = require('./admin/termconditionHeadings');
const users = require('./admin/users');
const partners = require('./admin/partners');

router.use('/', auth);
router.use('/news', news);
router.use('/testimonials', testimonials);
router.use('/blogs', blogs);
router.use('/articles', articles);
router.use('/offers', offers);
router.use('/faqs', faqs);
router.use('/settings', settings);
router.use('/home-banners', homeBanners);
router.use('/sub-headings', subHeadings);
router.use('/services-headings', servicesHeadings);
router.use('/network-headings', networkHeadings);

router.use('/camparison-headings', camparisonHeadings);
router.use('/our-network-headings', ourNetworkHeadings);
router.use('/about-us-headings', aboutUsHeadings);
router.use('/what-we-do-headings', doHeadings);
router.use('/mission-vision-headings', MVHeadings);
router.use('/cookie-policy-headings', cookiePolicyHeadings);
router.use('/privacy-policy-headings', privacyPolicyHeadings);
router.use('/term-policy-headings', termConditionHeadings);
router.use('/users', users);
router.use('/partners', partners);
module.exports = router;
