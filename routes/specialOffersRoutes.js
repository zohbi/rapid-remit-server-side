const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");

const Offers = require("../models/specialOffers");

const router = express.Router();

router
  .route("/")
  .get((req, res, next) => {
    Offers.find({
      expiryDate: { $gte: new Date() },
      createdAt: { $lte: new Date() },
    })
      .then(
        (resp) => {
          res.statusCode = 200;
          res.setHeader("Content-Type", "application/json");
          res.json(resp);
        },
        (err) => next(err)
      )
      .catch((err) => next(err));
  })
  .post((req, res, next) => {
    Offers.create(req.body).then((offer) => {
      res.statusCode = 200;
      res.setHeader("Content-Type", "application/json");
      res.json(offer);
    });
  });
router.put("/click/:Id", (req, res, next) => {
  req.body.isSelected = true;
  Offers.findByIdAndUpdate(
    req.params.Id,
    {
      $set: req.body,
    },
    { new: true }
  )
    .then(
      (resp) => {
        res.statusCode = 200;
        res.setHeader("Content-Type", "application/json");
        res.json(resp);
      },
      (err) => next(err)
    )
    .catch((err) => next(err));
});

module.exports = router;
