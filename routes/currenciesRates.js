const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const Rate = require('../models/rates');
const currency = require('../models/currency');
const authenticate = require('../authenticate_partner');
const Log = require('../models/rateLoges');
const DATEOBJ = require('date-and-time');
const PartnerRates = require('../models/partnerrates');
const Loges = require('../models/rateLoges');

const io = require('socket.io-client');
var socket = io.connect('http://localhost:4000/');

const router = express.Router();

router.use(bodyParser.json());

router.get('/:partner_name', async (req, res, next) => {
  try {
    let rates = await Rate.find({
      partner: req.params.partner_name.split(' ').join('').toLowerCase(),
    }).lean();

    res.statusCode = 200;
    res.setHeader('Content-Type', 'application/json');
    res.json(rates);
  } catch (e) {
    console.log(e);
  }
});

router.post(
  '/add/:uid/:name',
  authenticate.verifyUser,
  async (req, res, next) => {
    const id = req.params.uid;
    const name = req.params.name.split(' ').join('').toLowerCase();
    if (id) {
      try {
        const rates = await Rate.create({
          partner: name,
          base_currency: 'AED',
          country_name: req.body.country_name,
          rate: req.body.rate,
        });

        const Partnerrates = await PartnerRates.create({
          partner_name: req.params.name,
          base_currency: 'AED',
          country_name: req.body.country_name,
          rate: req.body.rate,
          partner_id: id,
        });

        const LogesRates = await Loges.create({
          partner: req.params.name,
          base_currency: 'AED',
          country_name: req.body.country_name,
          rate: req.body.rate,
        });

        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json({ success: true, data: Partnerrates });
      } catch (e) {
        console.log(e);
      }
    } else {
      res.statusCode = 400;
      res.setHeader('Content-Type', 'application/json');
      res.json({ success: false });
    }
  }
);

router.put(
  '/update/:uid/:name',
  authenticate.verifyUser,
  async (req, res, next) => {
    const id = req.params.uid;
    const name = req.params.name.split(' ').join('').toLowerCase();
    if (id) {
      try {
        const rates = await Rate.findByIdAndUpdate(req.body.currency_id, {
          country_name: req.body.country_name,
          rate: req.body.rate,
        });

        const Partnerrates = await PartnerRates.findOneAndUpdate(
          { partner_id: id, country_name: req.body.old_country_name },
          {
            country_name: req.body.country_name,
            rate: req.body.rate,
          }
        );
        const LogesRates = await Loges.create({
          partner: name,
          base_currency: 'AED',
          country_name: req.body.country_name,
          rate: req.body.rate,
        });
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json({ success: true, data: Partnerrates });
      } catch (e) {
        console.log(e);
      }
    } else {
      res.statusCode = 400;
      res.setHeader('Content-Type', 'application/json');
      res.json({ success: false });
    }
  }
);

router.delete(
  '/:uid/:currency_id/:country_name',
  authenticate.verifyUser,
  async (req, res, next) => {
    const id = req.params.uid;
    if (id) {
      try {
        const rates = await Rate.findByIdAndDelete(req.params.currency_id);
        const Partnerrates = await PartnerRates.findOneAndDelete({
          partner_id: id,
          country_name: req.params.country_name,
        });
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json({ success: true, data: Partnerrates });
      } catch (e) {
        console.log(e);
      }
    } else {
      res.statusCode = 400;
      res.setHeader('Content-Type', 'application/json');
      res.json({ success: false });
    }
  }
);
module.exports = router;
