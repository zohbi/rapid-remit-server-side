const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const Rates = require('../models/rates');

const axios = require('axios');
const Log = require('../models/rateLoges');
const DATEOBJ = require('date-and-time');
const io = require('socket.io-client');
var socket = io.connect('http://localhost:4000/');

const router = express.Router();

router.use(bodyParser.json());

// router.get("/check", async (req, res, next) => {
//   Log.find({ partner: "moneygram", country_name: "EGP" }).then((resp) => {
//     res.statusCode = 200;
//     res.setHeader("Content-Type", "application/json");
//     res.json(resp);
//   });
// });

router.post('/', async (req, res, next) => {
  let rates = new Rates(req.body);
  rates.save();

  res.statusCode = 200;
  res.setHeader('Content-Type', 'application/json');
  res.json(rates);
});

router.get('/', async (req, res, next) => {
  let rates = await Rates.find({
    partner: req.query.partner,
    base_currency: req.query.base_currency,
    country_name: req.query.country_name,
  });

  res.statusCode = 200;
  res.setHeader('Content-Type', 'application/json');
  res.json(rates);
});

const Loges = require('../models/rateLoges');

router.get('/fetch', async (req, res, next) => {
  let rates = await axios.get(
    'http://exchange.hostedsitedemo.com/exchange.php'
  );

  const partners = ['alansariexchange', 'xpressmoney', 'moneygram'];

  for (let i = 0; i < partners.length; i++) {
    let key = partners[i];
    const response = await new Promise(async (resolve, reject) => {
      try {
        await new Promise(async (resolve, reject) => {
          rates.data[key].map(async (rate) => {
            console.log('TRYING TO PUT VALUE', rate);
            const creating = await Loges.create({
              partner: key,
              base_currency: 'AED',
              country_name: rate.currency,
              rate: rate.rate ? rate.rate : 1,
            });

            console.log('Rate Added for now moving to ', creating);

            const _rate = await Rates.findOne({
              $and: [{ partner: key }, { country_name: rate.currency }],
            });

            if (_rate) {
              _rate.rate = rate.rate ? rate.rate : 1;
              _rate.base_currency = 'AED';
              _rate.save();
            } else {
              let _new = {
                partner: key,
                country_name: rate.currency,
                rate: rate.rate ? rate.rate : 1,
                base_currency: 'AED',
              };

              let newrate = new Rates(_new);
              newrate.save();
            }

            resolve(true);
          });
        });

        resolve({ success: true });
      } catch {
        resolve({ success: false });
      }
    });

    console.log('SUCCESS', response);
  }

  res.statusCode = 200;
  res.setHeader('Content-Type', 'application/json');
  res.json({ success: true });

  // rates.data["alansariexchange"].map((rate) => {

  // Loges.create({
  //   partner: "alansariexchange",
  //   base_currency: "AED",
  //   country_name: rate.currency,
  //   rate: rate.rate ? rate.rate : 1,
  // }).then((log) => {
  //   console.log("loges", log);
  // });

  //   Rates.findOneAndUpdate(
  //     { partner: "alansariexchange", country_name: rate.currency },
  //     {
  //       $set: { rate: rate.rate ? rate.rate : 1 },
  //     },
  //     { new: true }
  //   ).then((rate) => {
  //     //  console.log("rates", rate)
  //   });
  // });

  // rates.data["xpressmoney"].map((rate) => {
  //   //console.log("rayes", rate)
  //   Loges.create({
  //     partner: "xpressmoney",
  //     base_currency: "AED",
  //     country_name: rate.currency,
  //     rate: rate.rate ? rate.rate : 1,
  //   }).then((log) => {
  //     console.log("loges", log);
  //   });

  //   Rates.findOneAndUpdate(
  //     { partner: "xpressmoney", country_name: rate.currency },
  //     {
  //       $set: { rate: rate.rate ? rate.rate : 1 },
  //     },
  //     { new: true }
  //   ).then((rate) => {
  //     //  console.log("rates", rate)
  //   });
  // });

  // rates.data["moneygram"].map((rate) => {
  //   //console.log("rayes", rate)
  //   Loges.create({
  //     partner: "moneygram",
  //     base_currency: "AED",
  //     country_name: rate.currency,
  //     rate: rate.rate ? rate.rate : 1,
  //   }).then((log) => {
  //     console.log("loges", log);
  //   });

  //   Rates.findOneAndUpdate(
  //     { partner: "moneygram", country_name: rate.currency },
  //     {
  //       $set: { rate: rate.rate ? rate.rate : 1 },
  //     },
  //     { new: true }
  //   ).then((rate) => {
  //     //  console.log("rates", rate)
  //   });
  // });
});

router.post('/chart', async (req, res, next) => {
  //socket.emit("chart", req.body);
  console.log('start');
  const data = req.body;
  if (data.partners && data.time) {
    let date = new Date();
    console.log(
      'date',
      date.getDate(),
      DATEOBJ.addDays(date, -30),
      date.toString()
    );
    //let time = date.getDate() - parseInt(data.time);
    let time = DATEOBJ.addDays(date, -parseInt(data.time));
    let final = `${time.getFullYear()}-${
      time.getMonth() + 1
    }-${time.getDate()}`;
    //let finaDate = new Date(final);
    console.log('final', final);
    let array = [
      'AUD',
      'CAD',
      'EGP',
      'EUR',
      'GBP',
      'INR',
      'JOD',
      'LKR',
      'NPR',
      'PHP',
      'PKR',
      'USD',
    ];

    let data2 = [];
    for (let i = 0; i < array.length; i++) {
      let rates = await Log.find({
        partner: data.partners,
        country_name: array[i],
        createdAt: { $gte: final },
      })
        .sort({ createdAt: -1 })
        .select('rate , createdAt');
      //data2 = [...data2, rates];

      data2[i] = {
        [array[i]]: Array.from(
          new Set(rates.map((r) => r.createdAt.getDate()))
        ).map((d) => {
          return {
            label: d,
            y: rates.find((f) => f.createdAt.getDate() === d).rate,
          };
        }),
      };
    }
    console.log('data', data2);

    // let result = {
    //   [data.country_name]: Array.from(
    //     new Set(rates.map((r) => r.createdAt.getDate()))
    //   ).map((d) => {
    //     return {
    //       label: d,
    //       y: rates.find((f) => f.createdAt.getDate() === d).rate,
    //     };
    //   }),
    // };
    // console.log("rates", result);

    res.statusCode = 200;
    res.setHeader('Content-Type', 'application/json');
    res.json(data2);
  }
});

router.get('/all', async (req, res, next) => {
  let rates = await Rates.find({});

  res.statusCode = 200;
  res.setHeader('Content-Type', 'application/json');
  res.json(rates);
});

router.get('/data', async (req, res, next) => {
  let rates = await axios.get(
    'http://exchange.hostedsitedemo.com/exchange.php'
  );
  let addRates;

  console.log('rate', rates.data['moneygram'].length);
  rates.data['alansariexchange'].map((data) => {
    addRates = new Rates({
      partner: 'alansariexchange',
      base_currency: 'AED',
      country_name: data.currency,
      rate: data.rate,
    });
    addRates.save();
  });
  res.statusCode = 200;
  res.setHeader('Content-Type', 'application/json');
  res.json(addRates);
});

router.get('/currency', async (req, res, next) => {
  console.log('done');
  let data = await Rates.distinct('country_name');
  res.statusCode = 200;
  res.setHeader('Content-Type', 'application/json');
  res.json(data);
});
module.exports = router;
