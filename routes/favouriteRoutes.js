const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

const Fav = require('../models/favourite');
const middlewear = require('../middlewear');

const router = express.Router();

router.use(bodyParser.json());

router.get('/partner', async (req, res, next) => {
  let data = await Fav.find();
  // Fav.find({ user_id: req.params.id }).then(resp => {

  // });
  res.statusCode = 200;
  res.setHeader('Content-Type', 'application/json');
  res.json(data);
});

router.post('/:id', middlewear.checkToken, (req, res, next) => {
  if (req.params.id && req.body.fav_id) {
    Fav.find({ user_id: req.params.id, fav_id: req.body.fav_id }).then(
      (check) => {
        if (check.length == 0) {
          Fav.create({ user_id: req.params.id, fav_id: req.body.fav_id }).then(
            (resp) => {
              res.statusCode = 200;
              res.setHeader('Content-Type', 'application/json');
              res.json(resp);
            }
          );
        } else {
          res.statusCode = 500;
          res.setHeader('Content-Type', 'application/json');
          res.json({ err: 'already exsists ' });
        }
      }
    );
  } else {
    res.statusCode = 400;
    res.setHeader('Content-Type', 'application/json');
    res.json({ err: 'something is missing' });
  }
});

router.delete('/:id', middlewear.checkToken, (req, res, next) => {
  if (req.params.id && req.body.fav_id) {
    Fav.find({ user_id: req.params.id, fav_id: req.body.fav_id }).then(
      (check) => {
        if (check.length > 0) {
          Fav.findOneAndRemove({
            user_id: req.params.id,
            fav_id: req.body.fav_id,
          }).then((resp) => {
            res.statusCode = 200;
            res.setHeader('Content-Type', 'application/json');
            res.json(resp);
          });
        } else {
          res.statusCode = 500;
          res.setHeader('Content-Type', 'application/json');
          res.json({ err: 'Not exsists' });
        }
      }
    );
  } else {
    res.statusCode = 400;
    res.setHeader('Content-Type', 'application/json');
    res.json({ err: 'something is missing' });
  }
});

router.get('/:id', async (req, res, next) => {
  if (req.params.id) {
    let data = await Fav.aggregate([
      { $match: { user_id: mongoose.Types.ObjectId(req.params.id) } },
      {
        $lookup: {
          from: 'partners',
          localField: 'fav_id',
          foreignField: '_id',
          as: 'partners',
        },
      },
      {
        $lookup: {
          from: 'reviews',
          localField: 'fav_id',
          foreignField: 'partner_id',
          as: 'reviews',
        },
      },
    ]);
    // Fav.find({ user_id: req.params.id }).then(resp => {

    // });
    res.statusCode = 200;
    res.setHeader('Content-Type', 'application/json');
    res.json(data);
  } else {
    res.statusCode = 400;
    res.setHeader('Content-Type', 'application/json');
    res.json({ err: 'user_id is missing' });
  }
});

module.exports = router;
