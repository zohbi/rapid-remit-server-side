const express = require('express');
const setting = require('../models/setting');
const router = express.Router();

router.route('/').get((req, res, next) => {
  setting
    .findOne({})
    .then(
      (resp) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(resp);
      },
      (err) => next(err)
    )
    .catch((err) => next(err));
});

module.exports = router;
