const express = require("express");

const Footer = require("../models/footer");

const router = express.Router();

router.route("/").post((req, res, next) => {
  Footer.create(req.body)
    .then(
      (resp) => {
        res.statusCode = 200;
        res.setHeader("Content-Type", "application/json");
        res.json(resp);
      },
      (err) => next(err)
    )
    .catch((err) => next(err));
});

router.route("/").get((req, res, next) => {
  Footer.find()
    .then(
      (resp) => {
        res.statusCode = 200;
        res.setHeader("Content-Type", "application/json");
        res.json(resp);
      },
      (err) => next(err)
    )
    .catch((err) => next(err));
});

router.put("/:id", async (req, res) => {
  Footer.findByIdAndUpdate(
    req.params.id,
    {
      $set: req.body,
    },
    { new: true }
  ).then((resp) => {
    res.statusCode = 200;
    res.setHeader("Content-Type", "application/json");
    res.json(resp);
  });
});

module.exports = router;
