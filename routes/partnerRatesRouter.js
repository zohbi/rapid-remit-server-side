const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const PartnerRates = require('../models/partnerrates');
const PartnerLocation = require('../models/locations');

const boom = require('boom');
const Joi = require('@hapi/joi');
const Partner = require('../models/partners');
const authenticate = require('../authenticate_partner');
const router = express.Router();
router.use(bodyParser.json());
const Rates = require('../models/rates');
const Count = require('../models/counts');

const multer = require('multer');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, 'public/images');
  },

  filename: (req, file, cb) => {
    cb(null, Date.now() + '-' + file.originalname);
  },
});

const imageFileFilter = (req, file, cb) => {
  if (!file.originalname.match(/\.(JPG|jpeg|png|gif|jpg)$/)) {
    return cb(new Error('You can upload only image files!'), false);
  }
  cb(null, true);
};
const upload = multer({ storage: storage, fileFilter: imageFileFilter });

router.post('/image', upload.single('photo'), (req, res) => {
  res.statusCode = 200;
  res.setHeader('Content-Type', 'application/json');
  res.json(req.file);
});

router.get('/check', (req, res) => {
  var response = [];

  let data = req.query;
  var converted;
  let amount;
  //var amount = parseInt(req.query.amount);
  if (data.base_currency && data.country_name && data.amount) {
    var currency = [
      { name: 'Pakistan', currency: 'RS' },
      { name: 'USA', currency: 'USD' },
      { name: 'Pakistan', currency: 'PKR' },
      { name: 'Pakistan', currency: 'PKR' },
      { name: 'Pakistan', currency: 'PKR' },
      { name: 'Pakistan', currency: 'PKR' },
      { name: 'Pakistan', currency: 'PKR' },
      { name: 'Pakistan', currency: 'PKR' },
    ];
    let required = currency.filter((f) => f.name == data.country_name);

    const less_equal = 'LE';
    const greater_equal = 'GE';
    const less = 'L';
    const greater = 'G';

    data.limit ? data.limit : (data.limit = 10);
    data.sort ? data.sort : (data.sort = 1);

    PartnerRates.aggregate([
      { $match: { base_currency: data.base_currency } },
      { $match: { country_name: data.country_name } },
      data.name
        ? { $match: { partner_name: data.name } }
        : { $match: { base_currency: data.base_currency } },
      {
        $lookup: {
          from: 'partners',
          let: { partner_id: '$partner_id' },
          as: 'partner_details',
          pipeline: [
            {
              $match: {
                $expr: {
                  $and: [{ $eq: ['$_id', '$$partner_id'] }],
                },
              },
            },
          ],
        },
      },
      {
        $lookup: {
          from: 'reviews',
          localField: 'partner_id',
          foreignField: 'partner_id',
          as: 'reviews',
        },
      },
      {
        $skip:
          parseInt(data.page) * parseInt(data.limit) - parseInt(data.limit),
      },
      { $limit: parseInt(data.limit) },
      { $sort: { _id: parseInt(data.sort) } },
      //{ $project: { item: 1 , amount_recievable: { $add: [ "$payment_fees", "$rate"  ] } }
      //}
    ]).then((lookup) => {
      if (lookup.length == 0) {
        return res.json('No Record Found');
      }
      lookup.forEach((element, index) => {
        Rates.findOne({
          partner: element.partner_name,
          base_currency: data.base_currency,
          country_name: data.country_name,
        }).then((extra) => {
          let done = false;
          if (element.limit) {
            if (element.limit > parseInt(data.amount)) {
              done = true;
            }
          } else {
            done = true;
          }
          if (done) {
            let rateArray = [];
            //let extra = 41.92;
            // var condition1;
            // var condition2;

            element.rules.forEach((rule) => {
              switch (rule.operator) {
                case less_equal:
                  if (rule.amount >= parseInt(data.amount)) {
                    console.log('rate', rule.rate);
                    rateArray.push(rule.rate);
                  }
                  break;
                case greater:
                  if (rule.amount < parseInt(data.amount)) {
                    console.log('rate', rule.rate);
                    rateArray.push(rule.rate);
                  }
                  break;
                case less:
                  if (rule.amount > parseInt(data.amount)) {
                    console.log('rate', rule.rate);
                    rateArray.push(rule.rate);
                  }
                  break;
                case greater_equal:
                  if (rule.amount <= parseInt(data.amount)) {
                    console.log('rate', rule.rate);
                    rateArray.push(rule.rate);
                  }
                  break;
                default:
                  console.log('rate', rule.rate);
                  rateArray.push(rule.rate);

                  break;
              }
              //delete element['rules'];

              element['rate'] = Math.max.apply(null, rateArray);

              //console.log("check", condition1, condition2, extra);
              // condition1 == "sum"
              //   ? condition2 == "F"
              //     ? (amount = element["rate"] + extra)
              //     : null
              //   : condition2 == "F"
              //   ? ((amount = element["rate"] - extra), console.log("dabad"))
              //   : null;
            });
            element['rate'] = Math.max.apply(null, rateArray);
            amount = parseInt(data.amount) - element['rate'];
            element['currency'] =
              required[0] && required[0].currency ? required[0].currency : '';
            converted = amount * extra.rate;

            lookup[index]['amount_recievable'] =
              converted - element.payment_fees - element.taxes_rule;

            response.push(element);
          }

          res.statusCode = 200;
          res.setHeader('Content-Type', 'application/json');
          res.json(response);
        });
      });

      return 1;
    });
  } else {
    res.statusCode = 400;
    res.setHeader('Content-Type', 'application/json');
    res.json('some data is missing');
  }
});

router.get('/', async (req, res) => {
  let data = req.query;
  var amount = parseInt(req.query.amount);
  if (data.base_currency && data.country_name && data.amount) {
    data.limit ? data.limit : (data.limit = 10);
    data.sort ? data.sort : (data.sort = 1);

    const lookup = await PartnerRates.aggregate([
      {
        $match: {
          base_currency: data.base_currency,
          country_name: data.country_name,
        },
      },
      {
        $lookup: {
          from: 'partners',
          let: { partner_id: '$partner_id' },
          as: 'partner_details',
          pipeline: [
            {
              $match: {
                $expr: {
                  $and: [{ $eq: ['$_id', '$$partner_id'] }],
                },
              },
            },
          ],
        },
      },
      {
        $lookup: {
          from: 'reviews',
          localField: 'partner_id',
          foreignField: 'partner_id',
          as: 'reviews',
        },
      },
      // {
      //   $skip:
      //     parseInt(data.page) * parseInt(data.limit) - parseInt(data.limit),
      // },
      // { $limit: parseInt(data.limit) },
      // { $sort: { partner_name: parseInt(data.sort) } },
      { $project: { 'partner_details.salt': 0, 'partner_details.hash': 0 } },
    ]);
    if (lookup.length == 0) {
      return res.json('No Record Found');
    }

    let a = '';

    var data23;
    let ddf = [];
    for (let i = 0; i < lookup.length; i++) {
      let element = lookup[i];
      data23 = await Rates.findOne({
        partner: element.partner_name.toLowerCase().replace(/\s+/g, ''),
        base_currency: data.base_currency,
        country_name: data.country_name,
      });

      if (element.limit >= amount) {
        ddfObj = {
          ...element,
          rate: data23.rate,
          tax_deducted_check: 0,
          tax_deducted: 0,
        };
        if (element.tax_type == 'P') {
          ddfObj.fees_check =
            (element.payment_fees / 100) * (100 + element.taxes_rule);
        }
        if (element.tax_type == 'F') {
          ddfObj.fees_check = element.payment_fees + element.taxes_rule;
        }
        ddfObj.payment_fees = ddfObj.fees_check;
        if (element.rules.length > 0) {
          let taxesCheck = [];

          element.rules.forEach(function (value) {
            switch (value.operator) {
              case 'GE': {
                if (amount >= value.amount) {
                  if (value.condition2 == 'F') {
                    taxesCheck.push(+value.rate);
                  }
                  if (value.condition2 == 'P') {
                    const check = (amount / 100) * +value.rate;
                    taxesCheck.push(check);
                  }
                }
                break;
              }
              case 'G': {
                if (amount > +value.amount) {
                  if (value.condition2 == 'F') {
                    taxesCheck.push(+value.rate);
                  }
                  if (value.condition2 == 'P') {
                    const check = (amount / 100) * +value.rate;
                    taxesCheck.push(check);
                  }
                }
                break;
              }

              case 'LE': {
                if (amount <= +value.amount) {
                  if (value.condition2 == 'F') {
                    taxesCheck.push(+value.rate);
                  }
                  if (value.condition2 == 'P') {
                    const check = (amount / 100) * +value.rate;
                    taxesCheck.push(check);
                  }
                }
                break;
              }
              case 'L': {
                if (amount <= +value.amount) {
                  if (value.condition2 == 'F') {
                    taxesCheck.push(+value.rate);
                  }
                  if (value.condition2 == 'P') {
                    const check = (amount / 100) * +value.rate;
                    taxesCheck.push(check);
                  }
                }
                break;
              }
            }
          });
          let max = 0;
          if (taxesCheck.length > 0) {
            max = taxesCheck.reduce((max, n) => (n > max ? n : max));
          }
          ddfObj.tax_deducted_check = max;
          ddfObj.tax_deducted = max;
        }
        // const amount_final =
        //   amount - ddfObj.fees_check - ddfObj.tax_deducted_check;
        //ddfObj.amount_recievable = amount_final * data23.rate;
        ddfObj.amount_recievable = amount * data23.rate;

        let loca;
        if (
          data.longitute &&
          data.latitude &&
          data.longitute != 'undefined' &&
          data.latitude != 'undefined'
        ) {
          loca = await PartnerLocation.aggregate([
            {
              $geoNear: {
                near: {
                  type: 'Point',
                  coordinates: [+data.longitute, +data.latitude],
                },
                distanceField: 'dist.calculated',
                query: { partner_id: element.partner_id.toString() },
                spherical: true,
              },
            },
            { $limit: 1 },
            {
              $sort: { 'dist.calculated': 1 },
            },
          ]);
        } else {
          loca = await PartnerLocation.find({
            partner_id: element.partner_id.toString(),
          }).limit(1);
        }

        ddfObj.location = { ...loca };

        ddf.push(ddfObj);
      }
    }
    res.statusCode = 200;
    res.setHeader('Content-Type', 'application/json');
    res.json(ddf);
  } else {
    res.statusCode = 400;
    res.setHeader('Content-Type', 'application/json');
    res.json('some data is missing');
  }
});

router.get('/checking', async (req, res) => {
  //console.log("amount", parseInt(req.query.amount))
  var last = [];
  let data = req.query;
  var amount = parseInt(req.query.amount);
  if (data.base_currency && data.country_name && data.amount) {
    // var currency = [
    //   { name: "Pakistan", currency: "PKR" },
    //   { name: "USA", currency: "USD" },
    //   { name: "Pakistan", currency: "PKR" },
    //   { name: "Pakistan", currency: "PKR" },
    //   { name: "Pakistan", currency: "PKR" },
    //   { name: "Pakistan", currency: "PKR" },
    //   { name: "Pakistan", currency: "PKR" },
    //   { name: "Pakistan", currency: "PKR" }
    // ];
    // let required = currency.filter(f => f.name == data.country_name);
    let ccounts = await Count.findById('5ebb9a50bba9dd2c52dc53b2');
    console.log('ccounts', ccounts);
    if (ccounts.rates_count < 999) {
      let c = await Count.findByIdAndUpdate(
        '5ebb9a50bba9dd2c52dc53b2',
        {
          $inc: { rates_count: 1 },
        },
        { new: true }
      );
    }

    let response = [];
    const less_equal = 'LE';
    const greater_equal = 'GE';
    const less = 'L';
    const greater = 'G';
    var convertRate = [];
    let name2, name;
    data.name ? (name = new RegExp(data.name)) : null;
    console.log('batao', name);
    data.limit ? data.limit : (data.limit = 10);
    data.sort ? data.sort : (data.sort = 1);

    const lookup = await PartnerRates.aggregate([
      { $match: { base_currency: data.base_currency } },
      { $match: { country_name: data.country_name } },
      data.name
        ? { $match: { partner_name: { $regex: name, $options: 'i' } } } //{ $match: { partner_name: data.name } }/^w/i
        : { $match: { base_currency: data.base_currency } },
      {
        $lookup: {
          from: 'partners',
          let: { partner_id: '$partner_id' },
          as: 'partner_details',
          pipeline: [
            {
              $match: {
                $expr: {
                  $and: [{ $eq: ['$_id', '$$partner_id'] }],
                },
              },
            },
          ],
        },
      },
      {
        $lookup: {
          from: 'reviews',
          localField: 'partner_id',
          foreignField: 'partner_id',
          as: 'reviews',
        },
      },
      {
        $skip:
          parseInt(data.page) * parseInt(data.limit) - parseInt(data.limit),
      },
      { $limit: parseInt(data.limit) },
      { $sort: { partner_name: parseInt(data.sort) } },
      { $project: { 'partner_details.salt': 0, 'partner_details.hash': 0 } },
    ]);

    console.log('dta', lookup);
    if (lookup.length == 0) {
      return res.json('No Record Found');
    }

    let a = '';

    var data23;
    for (let i = 0; i < lookup.length; i++) {
      let element = lookup[i];
      console.log(
        'apple',
        element.partner_name.toLowerCase().replace(/\s+/g, '')
      );
      data23 = await Rates.findOne({
        partner: element.partner_name.toLowerCase().replace(/\s+/g, ''),
        base_currency: data.base_currency,
        country_name: data.country_name,
      });

      a = data23;
      if (a) {
        console.log('abbb', a);
        convertRate.push({ rate: a.rate, partner: a.partner });
      }
    }

    let final_convert_rate;
    lookup.forEach((element, index) => {
      final_convert_rate = convertRate.filter(
        (f) =>
          f.partner == element.partner_name.toLowerCase().replace(/\s+/g, '')
      );
      console.log('final_convert_rate', final_convert_rate, convertRate);
      let frate;
      if (final_convert_rate.length > 0) {
        frate = final_convert_rate[0].rate;
      }

      let done = false;
      if (element.limit) {
        if (element.limit > parseInt(data.amount)) {
          console.log('done');
          done = true;
        }
      } else {
        done = true;
      }

      let rateArray = [];
      let valuesToApply = [];
      var condition1;
      var condition2;
      var applicable = false;
      var rulesFiltered = [];

      if (done) {
        if (element.rules.length == 0) {
          console.log('convert rate', frate);
          element['rate'] = frate;
          let payment_fees = element.payment_fees * frate;
          let newA = parseFloat(data.amount) * frate; //element["rate"] * ;
          let tax = 0;
          console.log('newA', newA);
          // if (element.tax_type == "F") {
          //   newA = newA - parseFloat((element.taxes_rule*frate)) //parseInt(element.taxes_rule);
          //   tax = element.taxes_rule*frate;
          // } else {
          //   let deduct = (payment_fees / 100) * (element.taxes_rule*frate);
          //   tax =(payment_fees / 100) * (element.taxes_rule*frate);
          //   newA = newA - deduct;
          // }

          if (element.tax_type == 'F') {
            //not imp, another logic
            newA = newA - parseFloat(element.taxes_rule * frate);

            //what to do with tax here ? taxes_ruloe is value given , frate is rate partner providing
            tax = parseFloat(element.taxes_rule * frate);

            //this is for tax , payment_fees is variable having payment fees
            // tax = element.taxes_rule *payment_fees ; //payment_fees
          } else {
            // let deduct =
            //   parseFloat(data.amount) * ((element.taxes_rule / 100) * frate);
            // tax = deduct;
            // tax =  payment_fees*frate;
            let deduct;
            if (payment_fees > 0) {
              // deduct =  parseFloat(payment_fees) * ((element.taxes_rule / 100) * frate);
              let deduct =
                (parseFloat(payment_fees) * element.taxes_rule) / 100;
              tax = deduct;
            } else {
              deduct = 0;
              tax = deduct;
            }

            newA = newA - deduct;
          }

          element['payment_fees'] = element['payment_fees'] + tax / frate;

          element['amount_recievable'] = newA - payment_fees;
          element['tax_deducted'] = 0; //tax;
          console.log('element', element);
          if (
            element['rate'] > 0 &&
            element['amount_recievable'] > 0 &&
            element['payment_fees']
          ) {
            return last.push(element);
          }
        }

        element.rules.forEach((rule) => {
          console.log('rules', rule);
          switch (rule.operator) {
            case less_equal:
              // 17500 <= 18000
              if (parseInt(data.amount) <= rule.amount) {
                applicable = 'less_equal';
                console.log('rate', rule.rate);
                condition1 = rule.conditions;
                condition2 = rule.condition2;
                valuesToApply.push(rule.amount);
                rateArray.push(rule.rate);
                rulesFiltered.push(rule);
              }
              break;
            case greater:
              if (rule.amount < parseInt(data.amount)) {
                applicable = 'greater_equal';
                console.log('rate', rule.rate);
                condition1 = rule.conditions;
                condition2 = rule.condition2;
                valuesToApply.push(rule.amount);
                rateArray.push(rule.rate);
                rulesFiltered.push(rule);
              }
              break;
            case less:
              if (rule.amount > parseInt(data.amount)) {
                applicable = 'less_equal';
                console.log('rate', rule.rate);
                condition1 = rule.conditions;
                condition2 = rule.condition2;
                valuesToApply.push(rule.amount);
                rateArray.push(rule.rate);
                rulesFiltered.push(rule);
              }
              break;
            case greater_equal:
              if (rule.amount <= parseInt(data.amount)) {
                applicable = 'greater_equal';
                console.log('rate', rule.rate);
                condition1 = rule.conditions;
                condition2 = rule.condition2;
                valuesToApply.push(rule.amount);
                rateArray.push(rule.rate);
                rulesFiltered.push(rule);
              }
              break;
          }
          //delete element['rules'];

          // console.log("ee", element["amount_recievable"]);
        });

        console.log('RULES APPLICATBLE', valuesToApply, rateArray);

        valuesToApply.sort();

        var closest = false;
        if (valuesToApply.length > 0) {
          closest = valuesToApply.reduce(function (prev, curr) {
            return Math.abs(
              applicable === 'less_equal'
                ? curr - data.amount
                : Math.abs(curr - data.amount)
            ) < Math.abs(prev - data.amount)
              ? curr
              : prev;
          });
          console.log('CLOSEST RATE APPLIED', closest);
        }

        let rate = Math.min.apply(null, rateArray);
        if (closest) {
          rulesFiltered.forEach((rulebook) => {
            if (rulebook.amount === closest) {
              rate = rulebook.rate;
            }
          });
        }

        if (rulesFiltered.length === 0) {
          rate = 0;
        }

        console.log('RATE APPLIED', rate, frate);

        if (rate < 0) {
          console.log('------------------------', convertRate);
          element['rate'] = frate;
          let newA = element['rate'] * parseInt(data.amount);
          let payment_fees = element.payment_fees * frate;
          let tax_deducted = 0;
          if (element.tax_type == 'F') {
            newA = newA - parseInt(element.taxes_rule);
            tax_deducted = parseInt(element.taxes_rule);
          } else {
            //ese e apply horaha

            if (payment_fees > 0) {
              // let deduct = (payment_fees / 100) * element.taxes_rule;
              let deduct =
                (parseFloat(payment_fees) * element.taxes_rule) / 100;
              tax_deducted = deduct;
              newA = newA - deduct;
            } else {
              let deduct = 0;
              tax_deducted = 0;
              newA = newA;
            }
          }

          element['payment_fees'] =
            element['payment_fees'] + tax_deducted / frate;
          element['amount_recievable'] = newA - payment_fees;
          element['tax_deducted'] == 0; //tax_deducted;
          console.log('element', element);
          if (
            element['rate'] > 0 &&
            element['amount_recievable'] > 0 &&
            element['payment_fees']
          ) {
            return last.push(element);
          }
        }

        let amount;
        console.log('check', condition1, condition2, frate, rate, element._id);

        // if (condition1 == null || condition2 == null || rate < 0) {
        //   return;
        // }

        //4000-1*20.111
        let taxrules = 0;
        let payment_fees = element.payment_fees * frate;
        // if (element.tax_type == "F") {
        //   taxrules = element.taxes_rule;
        // } else {
        //   let deduct = (payment_fees / 100) * element.taxes_rule;
        //   taxrules = deduct;
        // }

        if (element.tax_type == 'F') {
          taxrules = element.taxes_rule * frate;
        } else {
          if (payment_fees > 0) {
            //  deduct =  parseFloat(payment_fees) * ((element.taxes_rule / 100) * frate);
            let deduct = (parseFloat(payment_fees) * element.taxes_rule) / 100; //parseFloat(payment_fees)*((element.taxes_rule / 100) * frate);
            // let deduct = parseFloat(data.amount) * (element.taxes_rule / 100) * frate; //((element.taxes_rule / parseFloat(data.amount))*100)*frate; //(payment_fees / 100) * element.taxes_rule;
            taxrules = deduct;
          } else {
            taxrules = 0;
          }
        }
        payment_fees = payment_fees + taxrules / frate;

        let valueOne = parseFloat(data.amount) * frate;
        let valueTwo = valueOne - payment_fees;
        let valueThree = valueTwo - taxrules;
        let final_amount = valueThree - rate * frate;

        console.log('AMOUNT ONE', valueOne);
        console.log('AMOUNT TWO', valueTwo);
        console.log('AMOUNT final_amount', final_amount);

        console.log('AMOUNT SEND', parseFloat(data.amount));
        console.log('AMOUNT FEES', element.payment_fees);
        console.log('AMOUNT frate', frate);
        console.log('AMOUNT rate', rate);

        console.log('TAX RULE DEDUCTED', taxrules);

        //  rate;

        // let final_amount =
        //    (parseInt(data.amount) - element.payment_fees) * rate;
        // let cRate = rate * frate;
        // let aa = parseInt(data.amount) * (cRate / 100);
        // condition1 == "sum"
        //   ? condition2 == "F"
        //     ? (amount = final_amount + cRate)
        //     : (amount = final_amount + parseInt(data.amount) * (cRate / 100))
        //   : condition2 == "F"
        //   ? final_amount > cRate
        //     ? (amount = final_amount - cRate)
        //     : (amount = 0)
        //   : convertRate[index] > aa
        //   ? (amount = frate - aa)
        //   : (amount = aa - frate);
        //let newAmount = parseInt(data.amount) * amount;
        element.payment_fees =
          parseFloat(element.payment_fees) + parseFloat(rate);
        console.log('amount', final_amount);
        element['rate'] = frate;
        element['amount_recievable'] = final_amount;
        element['currency'] = data.country_name;
        element['rate_applied'] = rate;
        element['tax_deducted'] = 0; //taxrules;

        console.log('tax_deducted ADDED');

        if (
          element &&
          element['rate'] >= 0 &&
          element['amount_recievable'] > 0
        ) {
          last.push(element);
        }
      }
      //console.log("last", element, index);
      last.push(element[index]);
    });

    //console.log("lecngth checl kari", frate, lookup.length);
    if (last.length == 0) {
      res.statusCode = 200;
      res.setHeader('Content-Type', 'application/json');
      res.json('No Record Found');
    } else {
      let final = last.filter((f) => f != null);
      res.statusCode = 200;
      res.setHeader('Content-Type', 'application/json');
      console.log('DATA TO SEND TO PACKET', final);
      // await PartnerLocation.ensureIndexes({
      //   "location.coordinates": "2dsphere",
      // });

      let recordsNear = [];
      for (let i = 0; i < final.length; i++) {
        // const loca = await PartnerLocation.findOne({
        //   location: {
        //     $near: {
        //       $geometry: { type: "Point", coordinates: [-73.9667, 40.78] },

        //     },
        //   },
        // });

        // const loca = await PartnerLocation.find({
        //   location: {
        //     $near: {
        //       $geometry: {
        //         type: 'Point',
        //         coordinates: [30.564058, 76.44762696],
        //       },
        //       distanceField: 'distance',
        //     },
        //   },
        //   partner_id: final[i].partner_id.toString(),
        // });
        let loca;
        if (
          data.longitute &&
          data.latitude &&
          data.longitute != 'undefined' &&
          data.latitude != 'undefined'
        ) {
          loca = await PartnerLocation.aggregate([
            {
              $geoNear: {
                near: {
                  type: 'Point',
                  coordinates: [+data.longitute, +data.latitude],
                },
                distanceField: 'dist.calculated',
                query: { partner_id: final[i].partner_id.toString() },
                spherical: true,
              },
            },
            { $limit: 1 },
            {
              $sort: { 'dist.calculated': 1 },
            },
          ]);
        } else {
          loca = await PartnerLocation.find({
            partner_id: final[i].partner_id.toString(),
          }).limit(1);
        }
        console.log('loca asd' + JSON.stringify(loca));
        if (loca) {
          final[i].location = loca;
        }
        // res.json(loca);
      }

      res.json(final);
    }
  } else {
    res.statusCode = 400;
    res.setHeader('Content-Type', 'application/json');
    res.json('some data is missing');
  }
});

router.get('/logedin', authenticate.verifyUser, async (req, res) => {
  console.log('ig', req.query.user_id);
  const lookup = await PartnerRates.aggregate([
    { $match: { partner_id: mongoose.Types.ObjectId(req.query.user_id) } },
    {
      $lookup: {
        from: 'reviews',
        localField: 'partner_id',
        foreignField: 'partner_id',
        as: 'reviews',
      },
    },
  ]);
  console.log('WE ARE NEW ' + lookup);
  res.statusCode = 200;
  res.setHeader('Content-Type', 'application/json');
  res.json(lookup);
});

router.get('/:id', async (req, res) => {
  if (req.params.id && req.params.id.length == 24) {
    const lookup = await PartnerRates.aggregate([
      { $match: { _id: mongoose.Types.ObjectId(req.params.id) } },
    ]);
    res.statusCode = 200;
    res.setHeader('Content-Type', 'application/json');
    res.json(lookup);
  } else {
    res.statusCode = 400;
    res.setHeader('Content-Type', 'application/json');
    res.json('Invalid Id');
  }
});

router.delete('/:id', async (req, res) => {
  if (req.params.id && req.params.id.length == 24) {
    PartnerRates.findByIdAndRemove(req.params.id).then((resp) => {
      res.statusCode = 200;
      res.setHeader('Content-Type', 'application/json');
      res.json(resp);
    });
  } else {
    res.statusCode = 400;
    res.setHeader('Content-Type', 'application/json');
    res.json('Invalid Id');
  }
});

router.post('/', authenticate.verifyUser, (req, res, next) => {
  const data = req.body;
  console.log('datadone', data);

  const schema = Joi.object().keys({
    country_name: Joi.string().required(),
    base_currency: Joi.string().min(3),
    transfer_time: Joi.string(),
    transfer_method: Joi.array(),
    payment_methods: Joi.array(),
    payment_fees: Joi.number(),
    taxes_rule: Joi.number(),
    rules: Joi.array(),
    partner_name: Joi.string(),
    limit: Joi.number(),
  });

  const val = schema.validate(data);
  if (!val.error) {
    Partner.find({
      name: data.partner_name,
      base_currency: data.base_currency,
      country_name: data.country_name,
    }).then((check) => {
      if (check.length > 0) {
        return res.status(422).json('Already exsits');
      }
    });
    Partner.find({ name: data.partner_name }).then((partner) => {
      if (partner.length >= 1) {
        data.partner_id = partner[0]._id;
        PartnerRates.create(data)
          .then(
            (resp) => {
              res.statusCode = 200;
              res.setHeader('Content-Type', 'application/json');
              res.json(resp);
            },
            (err) => next(err)
          )
          .catch((err) => next(err));
      } else {
        res.statusCode = 400;
        res.setHeader('Content-Type', 'application/json');
        res.json('no partner of this name');
      }
    });
  } else {
    res.statusCode = 400;
    res.setHeader('Content-Type', 'application/json');
    res.json(val.error);
  }
});

// router.put('/:id', authenticate.verifyUser, (req, res, next) => {
//   const data = req.body;
//   console.log('run');
//   if (req.params.id && req.params.id.length == 24) {
//     const schema = Joi.object().keys({
//       country_name: Joi.string().required(),
//       base_currency: Joi.string().min(3),
//       transfer_time: Joi.string(),
//       transfer_method: Joi.array(),
//       payment_methods: Joi.array(),
//       payment_fees: Joi.number(),
//       taxes_rule: Joi.number(),
//       tax_type: Joi.string().valid('F', 'P'),
//       rules: Joi.array(),
//       partner_name: Joi.string(),
//       _id: Joi.string(),
//       limit: Joi.number(),
//     });

//     const val = schema.validate(data);
//     if (!val.error) {
//       console.log('run', data);
//       // PartnerRates.find({
//       //   partner_name: data.partner_name,
//       //   base_currency: data.base_currency,
//       //   country_name: data.country_name
//       // }).then(check => {
//       //   console.log("check", check, check.length);
//       //   // if (check.length > 0) {
//       //   //   return res.status(422).json("Already exsits");
//       //   // }
//       // });
//       // PartnerRates.find({ name: data.partner_name }).then(partner => {
//       //   if (partner.length >= 1) {
//       //    data.partner_id = partner[0]._id;
//       PartnerRates.findByIdAndUpdate(
//         req.params.id,
//         {
//           $set: data,
//         },
//         { new: true }
//       )
//         .then(
//           (resp) => {
//             console.log('reates', resp);
//             res.statusCode = 200;
//             res.setHeader('Content-Type', 'application/json');
//             res.json(resp);
//           },
//           (err) => next(err)
//         )
//         .catch((err) => next(err));

//       // });
//     } else {
//       res.statusCode = 400;
//       res.setHeader('Content-Type', 'application/json');
//       res.json(val.error);
//     }
//   } else {
//     res.statusCode = 400;
//     res.setHeader('Content-Type', 'application/json');
//     res.json('Invalid Id');
//   }
// });
router.put('/:id', authenticate.verifyUser, (req, res, next) => {
  const data = req.body;
  console.log('run');
  if (req.params.id && req.params.id.length == 24) {
    // PartnerRates.find({
    //   partner_name: data.partner_name,
    //   base_currency: data.base_currency,
    //   country_name: data.country_name
    // }).then(check => {
    //   console.log("check", check, check.length);
    //   // if (check.length > 0) {
    //   //   return res.status(422).json("Already exsits");
    //   // }
    // });
    // PartnerRates.find({ name: data.partner_name }).then(partner => {
    //   if (partner.length >= 1) {
    //    data.partner_id = partner[0]._id;
    PartnerRates.findByIdAndUpdate(
      req.params.id,
      {
        $set: data,
      },
      { new: true }
    )
      .then(
        (resp) => {
          console.log('reates', resp);
          res.statusCode = 200;
          res.setHeader('Content-Type', 'application/json');
          res.json(resp);
        },
        (err) => next(err)
      )
      .catch((err) => next(err));

    // });
  } else {
    res.statusCode = 400;
    res.setHeader('Content-Type', 'application/json');
    res.json('Invalid Id');
  }
});

module.exports = router;
