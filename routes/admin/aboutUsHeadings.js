const express = require('express');
const router = express.Router();
const AboutUsHeadings = require('../../models/aboutUsHeading');
const { adminCheckToken } = require('../../middlewear');
const { body, validationResult } = require('express-validator');

router.get('/', adminCheckToken, async (req, res) => {
  try {
    const aboutUsHeadings = await AboutUsHeadings.findOne({}).lean();

    return res.status(200).json({
      status: true,
      statusCode: 200,
      message: 'get about us heading data successfully',
      data: {
        aboutUsHeadings,
      },
    });
  } catch (error) {
    return res.status(400).json({
      status: false,
      statusCode: 400,
      message: error.message,
      data: {},
    });
  }
});

router.post(
  '/',
  adminCheckToken,
  body('heading', 'Heading must be specified.')
    .not()
    .isEmpty()
    .withMessage('Heading be not empty.'),

  body('sub_heading', 'Sub Heading must be specified.')
    .not()
    .isEmpty()
    .withMessage('Sub Heading be not empty.'),

  body('text', 'Text must be specified.')
    .not()
    .isEmpty()
    .withMessage('Text be not empty.'),

  async (req, res) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        const error = errors.array();
        return res.status(400).json({
          status: false,
          statusCode: 400,
          message: error[0].msg,
          data: {},
        });
      }

      const aboutUsHeadings = await AboutUsHeadings.findOneAndUpdate(
        {},
        req.body,
        {
          new: true,
        }
      );

      return res.status(200).json({
        status: true,
        statusCode: 200,
        message: 'about us heading updated successfully',
        data: {
          aboutUsHeadings,
        },
      });
    } catch (error) {
      return res.status(400).json({
        status: false,
        statusCode: 400,
        message: error.message,
        data: {},
      });
    }
  }
);

module.exports = router;
