const express = require('express');
const router = express.Router();
const Users = require('../../models/user');
const { adminCheckToken } = require('../../middlewear');
const { body, validationResult } = require('express-validator');

router.get('/', adminCheckToken, async (req, res) => {
  try {
    const users = await Users.find({}).lean();

    return res.status(200).json({
      status: true,
      statusCode: 200,
      message: 'get users data successfully',
      data: {
        users,
      },
    });
  } catch (error) {
    return res.status(400).json({
      status: false,
      statusCode: 400,
      message: error.message,
      data: {},
    });
  }
});

router.post('/status/:id', adminCheckToken, async (req, res) => {
  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      const error = errors.array();
      return res.status(400).json({
        status: false,
        statusCode: 400,
        message: error[0].msg,
        data: {},
      });
    }
    const user = await Users.findById(req.params.id);
    const users = await Users.findOneAndUpdate(
      {},
      { status: !user.status },
      {
        new: true,
      }
    );

    return res.status(200).json({
      status: true,
      statusCode: 200,
      message: 'Users status updated successfully',
      data: {
        users,
      },
    });
  } catch (error) {
    return res.status(400).json({
      status: false,
      statusCode: 400,
      message: error.message,
      data: {},
    });
  }
});

module.exports = router;
