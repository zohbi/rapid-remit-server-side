const express = require('express');
const router = express.Router();
const Articles = require('../../models/Articles');
const { adminCheckToken } = require('../../middlewear');
const { body, validationResult } = require('express-validator');

router.get('/', adminCheckToken, async (req, res) => {
  try {
    const articles = await Articles.find({}).lean();

    return res.status(200).json({
      status: true,
      statusCode: 200,
      message: 'get all articles successfully',
      data: {
        articles,
      },
    });
  } catch (error) {
    return res.status(400).json({
      status: false,
      statusCode: 400,
      message: error.message,
      data: {},
    });
  }
});

router.post(
  '/',
  adminCheckToken,
  body('bannerTitle', 'Banner Title must be specified.')
    .not()
    .isEmpty()
    .withMessage('Banner Title must be not empty.'),
  body('title', 'Title must be specified.')
    .not()
    .isEmpty()
    .withMessage('Title must be not empty.'),
  body('text', 'Text must be specified.')
    .not()
    .isEmpty()
    .withMessage('Text must be not empty.'),
  body('img', 'Image must be specified.')
    .not()
    .isEmpty()
    .withMessage('Image must be not empty.'),
  async (req, res) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        const error = errors.array();
        return res.status(400).json({
          status: false,
          statusCode: 400,
          message: error[0].msg,
          data: {},
        });
      }

      const articles = await Articles.create({
        bannerTitle: req.body.bannerTitle,
        title: req.body.title,
        text: req.body.text,
        img: req.body.img,
      });

      res.status(201).json({
        status: true,
        statusCode: 201,
        message: 'articles created successfully',
        data: {
          articles,
        },
      });
    } catch (error) {
      return res.status(400).json({
        status: false,
        statusCode: 400,
        message: error.message,
        data: {},
      });
    }
  }
);

router.get('/:id', adminCheckToken, async (req, res) => {
  try {
    const articles = await Articles.findById(req.params.id);

    if (articles != null) {
      return res.status(200).json({
        status: true,
        statusCode: 200,
        message: 'articles get successfully',
        data: {
          articles,
        },
      });
    }

    return res.status(404).json({
      status: false,
      statusCode: 404,
      message: 'articles not found',
      data: {},
    });
  } catch (error) {
    return res.status(400).json({
      status: false,
      statusCode: 400,
      message: error.message,
      data: {},
    });
  }
});

router.put(
  '/:id',
  adminCheckToken,
  body('bannerTitle', 'Banner Title must be specified.')
    .not()
    .isEmpty()
    .withMessage('Banner Title must be not empty.'),
  body('title', 'Title must be specified.')
    .not()
    .isEmpty()
    .withMessage('Title must be not empty.'),
  body('text', 'Text must be specified.')
    .not()
    .isEmpty()
    .withMessage('Text must be not empty.'),
  body('img', 'Image must be specified.')
    .not()
    .isEmpty()
    .withMessage('Image must be not empty.'),
  async (req, res) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        const error = errors.array();
        return res.status(400).json({
          status: false,
          statusCode: 400,
          message: error[0].msg,
          data: {},
        });
      }

      const articles = await Articles.findByIdAndUpdate(
        req.params.id,
        {
          bannerTitle: req.body.bannerTitle,
          title: req.body.title,
          text: req.body.text,
          img: req.body.img,
        },
        { new: true }
      );

      if (articles != null) {
        return res.status(200).json({
          status: true,
          statusCode: 200,
          message: 'articles updated successfully',
          data: {
            articles,
          },
        });
      }

      return res.status(404).json({
        status: false,
        statusCode: 404,
        message: 'articles not found',
        data: {},
      });
    } catch (error) {
      return res.status(400).json({
        status: false,
        statusCode: 400,
        message: error.message,
        data: {},
      });
    }
  }
);

router.delete('/:id', adminCheckToken, async (req, res) => {
  try {
    const articles = await Articles.findByIdAndRemove(req.params.id);

    if (articles != null) {
      return res.status(200).json({
        status: true,
        statusCode: 200,
        message: 'articles deleted successfully',
        data: {
          articles,
        },
      });
    }

    return res.status(404).json({
      status: false,
      statusCode: 404,
      message: 'articles not found',
      data: {},
    });
  } catch (error) {
    return res.status(400).json({
      status: false,
      statusCode: 400,
      message: error.message,
      data: {},
    });
  }
});

module.exports = router;
