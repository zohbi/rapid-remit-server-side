const express = require('express');
const router = express.Router();
const CookiePolicyHeadings = require('../../models/cookiePolicyHeading');
const { adminCheckToken } = require('../../middlewear');
const { body, validationResult } = require('express-validator');

router.get('/', adminCheckToken, async (req, res) => {
  try {
    const cookiePolicyHeadings = await CookiePolicyHeadings.findOne({}).lean();

    res.status(200).json({
      status: true,
      statusCode: 200,
      message: 'get cookie policy heading data successfully',
      data: {
        cookiePolicyHeadings,
      },
    });
  } catch (error) {
    return res.status(400).json({
      status: false,
      statusCode: 400,
      message: error.message,
      data: {},
    });
  }
});

router.post(
  '/',
  adminCheckToken,
  body('text', 'Text must be specified.')
    .not()
    .isEmpty()
    .withMessage('Text be not empty.'),

  async (req, res) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        const error = errors.array();
        return res.status(400).json({
          status: false,
          statusCode: 400,
          message: error[0].msg,
          data: {},
        });
      }

      const cookiePolicyHeadings = await CookiePolicyHeadings.findOneAndUpdate(
        {},
        req.body,
        {
          new: true,
        }
      );

      res.status(200).json({
        status: true,
        statusCode: 200,
        message: 'cookie policy heading updated successfully',
        data: {
          cookiePolicyHeadings,
        },
      });
    } catch (error) {
      return res.status(400).json({
        status: false,
        statusCode: 400,
        message: error.message,
        data: {},
      });
    }
  }
);

module.exports = router;
