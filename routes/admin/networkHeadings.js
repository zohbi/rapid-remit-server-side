const express = require('express');
const router = express.Router();
const NetworkHeadings = require('../../models/networkHeading');
const { adminCheckToken } = require('../../middlewear');
const { body, validationResult } = require('express-validator');

router.get('/', adminCheckToken, async (req, res) => {
  try {
    const networkHeadings = await NetworkHeadings.findOne({}).lean();

    return res.status(200).json({
      status: true,
      statusCode: 200,
      message: 'get network headings data successfully',
      data: {
        networkHeadings,
      },
    });
  } catch (error) {
    return res.status(400).json({
      status: false,
      statusCode: 400,
      message: error.message,
      data: {},
    });
  }
});

router.post(
  '/',
  adminCheckToken,
  body('text', 'Text must be specified.')
    .not()
    .isEmpty()
    .withMessage('Text be not empty.'),

  async (req, res) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        const error = errors.array();
        return res.status(400).json({
          status: false,
          statusCode: 400,
          message: error[0].msg,
          data: {},
        });
      }

      const networkHeadings = await NetworkHeadings.findOneAndUpdate(
        {},
        req.body,
        {
          new: true,
        }
      );

      return res.status(200).json({
        status: true,
        statusCode: 200,
        message: 'Network Headings updated successfully',
        data: {
          networkHeadings,
        },
      });
    } catch (error) {
      return res.status(400).json({
        status: false,
        statusCode: 400,
        message: error.message,
        data: {},
      });
    }
  }
);

module.exports = router;
