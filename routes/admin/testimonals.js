const express = require('express');
const router = express.Router();
const Testimonials = require('../../models/testimonial');
const { adminCheckToken } = require('../../middlewear');
const { body, validationResult } = require('express-validator');

router.get('/', adminCheckToken, async (req, res) => {
  try {
    const testimonials = await Testimonials.find({}).lean();

    return res.status(200).json({
      status: true,
      statusCode: 200,
      status: 'get all testimonials successfully',
      data: {
        testimonials,
      },
    });
  } catch (error) {
    return res.status(400).json({
      status: false,
      statusCode: 400,
      message: error.message,
      data: {},
    });
  }
});

router.post(
  '/',
  adminCheckToken,
  body('name', 'Name must be specified.')
    .not()
    .isEmpty()
    .withMessage('Name must be not empty.'),

  body('text', 'Text must be specified.')
    .not()
    .isEmpty()
    .withMessage('Text must be not empty.'),

  body('img', 'Image must be specified.')
    .not()
    .isEmpty()
    .withMessage('Image must be not empty.'),
  async (req, res) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        const error = errors.array();
        return res.status(400).json({
          status: false,
          statusCode: 400,
          message: error[0].msg,
          data: {},
        });
      }

      const testimonials = await Testimonials.create({
        name: req.body.name,
        text: req.body.text,
        img: req.body.img,
      });

      return res.status(201).json({
        status: true,
        statusCode: 201,
        status: 'testimonials created successfully',
        data: {
          testimonials,
        },
      });
    } catch (error) {
      return res.status(400).json({
        status: false,
        statusCode: 400,
        message: error.message,
        data: {},
      });
    }
  }
);

router.get('/:id', adminCheckToken, async (req, res) => {
  try {
    const testimonials = await Testimonials.findById(req.params.id);

    if (testimonials != null) {
      return res.status(200).json({
        status: true,
        statusCode: 200,
        status: 'testimonials get successfully',
        data: {
          testimonials,
        },
      });
    }

    return res.status(404).json({
      status: false,
      statusCode: 404,
      message: 'testimonials not found',
      data: {},
    });
  } catch (error) {
    return res.status(400).json({
      status: false,
      statusCode: 400,
      message: error.message,
      data: {},
    });
  }
});

router.put(
  '/:id',
  adminCheckToken,
  body('name', 'Name must be specified.')
    .not()
    .isEmpty()
    .withMessage('Name must be not empty.'),

  body('text', 'Text must be specified.')
    .not()
    .isEmpty()
    .withMessage('Text must be not empty.'),

  body('img', 'Image must be specified.')
    .not()
    .isEmpty()
    .withMessage('Image must be not empty.'),
  async (req, res) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        const error = errors.array();
        return res.status(400).json({
          status: false,
          statusCode: 400,
          message: error[0].msg,
          data: {},
        });
      }

      const testimonials = await Testimonials.findByIdAndUpdate(
        req.params.id,
        {
          name: req.body.name,
          text: req.body.text,
          img: req.body.img,
        },
        { new: true }
      );

      if (testimonials != null) {
        return res.status(200).json({
          status: true,
          statusCode: 200,
          message: 'testimonials updated successfully',
          data: {
            testimonials,
          },
        });
      }

      return res.status(404).json({
        status: false,
        statusCode: 404,
        message: 'testimonials not found',
        data: {},
      });
    } catch (error) {
      return res.status(400).json({
        status: false,
        statusCode: 400,
        message: error.message,
        data: {},
      });
    }
  }
);

router.delete('/:id', adminCheckToken, async (req, res) => {
  try {
    const testimonials = await Testimonials.findByIdAndRemove(req.params.id);

    if (testimonials != null) {
      return res.status(200).json({
        status: true,
        statusCode: 200,
        status: 'testimonials deleted successfully',
        data: {
          testimonials,
        },
      });
    }

    return res.status(404).json({
      status: false,
      statusCode: 404,
      message: 'testimonials not found',
      data: {},
    });
  } catch (error) {
    return res.status(400).json({
      status: false,
      statusCode: 400,
      message: error.message,
      data: {},
    });
  }
});

module.exports = router;
