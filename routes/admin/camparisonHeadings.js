const express = require('express');
const router = express.Router();
const CamparisonHeadings = require('../../models/camparisonHeading');
const { adminCheckToken } = require('../../middlewear');
const { body, validationResult } = require('express-validator');

router.get('/', adminCheckToken, async (req, res) => {
  try {
    const camparisonHeadings = await CamparisonHeadings.findOne({}).lean();

    res.status(200).json({
      status: true,
      statusCode: 200,
      message: 'get camparison heading data successfully',
      data: {
        camparisonHeadings,
      },
    });
  } catch (error) {
    return res.status(400).json({
      status: false,
      statusCode: 400,
      message: error.message,
      data: {},
    });
  }
});

router.post(
  '/',
  adminCheckToken,
  body('text', 'Text must be specified.')
    .not()
    .isEmpty()
    .withMessage('Text be not empty.'),

  async (req, res) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        const error = errors.array();
        return res.status(400).json({
          status: false,
          statusCode: 400,
          message: error[0].msg,
          data: {},
        });
      }

      const camparisonHeadings = await CamparisonHeadings.findOneAndUpdate(
        {},
        req.body,
        {
          new: true,
        }
      );

      res.status(200).json({
        status: true,
        statusCode: 200,
        message: 'camparison heading updated successfully',
        data: {
          camparisonHeadings,
        },
      });
    } catch (error) {
      return res.status(400).json({
        status: false,
        statusCode: 400,
        message: error.message,
        data: {},
      });
    }
  }
);

module.exports = router;
