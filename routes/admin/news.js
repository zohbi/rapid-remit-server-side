const express = require('express');
const router = express.Router();
const News = require('../../models/news');
const { adminCheckToken } = require('../../middlewear');
const { body, validationResult } = require('express-validator');

router.get('/', adminCheckToken, async (req, res) => {
  try {
    const news = await News.find({}).lean();

    return res.status(200).json({
      status: true,
      statusCode: 200,
      message: 'get all news successfully',
      data: {
        news,
      },
    });
  } catch (error) {
    return res.status(400).json({
      status: false,
      statusCode: 400,
      message: error.message,
      data: {},
    });
  }
});

router.post(
  '/',
  adminCheckToken,
  body('heading', 'Heading must be specified.')
    .not()
    .isEmpty()
    .withMessage('Heading must be not empty.'),

  body('news', 'News must be specified.')
    .not()
    .isEmpty()
    .withMessage('News must be not empty.'),
  async (req, res) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        const error = errors.array();
        return res.status(400).json({
          status: false,
          statusCode: 400,
          message: error[0].msg,
          data: {},
        });
      }

      const news = await News.create({
        heading: req.body.heading,
        news: req.body.news,
      });

      return res.status(201).json({
        status: true,
        statusCode: 201,
        message: 'news created successfully',
        data: {
          news,
        },
      });
    } catch (error) {
      return res.status(400).json({
        status: false,
        statusCode: 400,
        message: error.message,
        data: {},
      });
    }
  }
);

router.get('/:id', adminCheckToken, async (req, res) => {
  try {
    const news = await News.findById(req.params.id);

    if (news != null) {
      return res.status(200).json({
        status: true,
        statusCode: 200,
        message: 'news get successfully',
        data: {
          news,
        },
      });
    }

    return res.status(404).json({
      status: false,
      statusCode: 404,
      message: 'news not found',
      data: {},
    });
  } catch (error) {
    return res.status(400).json({
      status: false,
      statusCode: 400,
      message: error.message,
      data: {},
    });
  }
});

router.put(
  '/:id',
  adminCheckToken,
  body('heading', 'Heading must be specified.')
    .not()
    .isEmpty()
    .withMessage('Heading must be not empty.'),

  body('news', 'News must be specified.')
    .not()
    .isEmpty()
    .withMessage('News must be not empty.'),
  async (req, res) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        const error = errors.array();
        return res.status(400).json({
          status: false,
          statusCode: 400,
          message: error[0].msg,
          data: {},
        });
      }

      const news = await News.findByIdAndUpdate(
        req.params.id,
        {
          heading: req.body.heading,
          news: req.body.news,
        },
        { new: true }
      );

      if (news != null) {
        return res.status(200).json({
          status: true,
          statusCode: 200,
          message: 'news updated successfully',
          data: {
            news,
          },
        });
      }

      return res.status(404).json({
        status: false,
        statusCode: 404,
        message: 'news not found',
        data: {},
      });
    } catch (error) {
      return res.status(400).json({
        status: false,
        statusCode: 400,
        message: error.message,
        data: {},
      });
    }
  }
);

router.delete('/:id', adminCheckToken, async (req, res) => {
  try {
    const news = await News.findByIdAndRemove(req.params.id);

    if (news != null) {
      return res.status(200).json({
        status: true,
        statusCode: 200,
        message: 'news deleted successfully',
        data: {
          news,
        },
      });
    }

    return res.status(404).json({
      status: false,
      statusCode: 404,
      message: 'news not found',
      data: {},
    });
  } catch (error) {
    return res.status(400).json({
      status: false,
      statusCode: 400,
      message: error.message,
      data: {},
    });
  }
});

module.exports = router;
