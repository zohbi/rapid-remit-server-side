const express = require('express');
const router = express.Router();
const Partners = require('../../models/partners');
const { adminCheckToken } = require('../../middlewear');
const { body, validationResult } = require('express-validator');

router.get('/', adminCheckToken, async (req, res) => {
  try {
    const partners = await Partners.find({}).lean();

    return res.status(200).json({
      status: true,
      statusCode: 200,
      message: 'get all partners successfully',
      data: {
        partners,
      },
    });
  } catch (error) {
    return res.status(400).json({
      status: false,
      statusCode: 400,
      message: error.message,
      data: {},
    });
  }
});

router.post(
  '/',
  adminCheckToken,
  body('email', 'Email must be specified.')
    .not()
    .isEmpty()
    .withMessage('Email must be not empty.')
    .isEmail()
    .withMessage('Email must be a valid email address.'),
  body('about_exchange', 'About Exchange must be specified.')
    .not()
    .isEmpty()
    .withMessage('About Exchange must be not empty.'),
  body('company_type', 'Company Type must be specified.')
    .not()
    .isEmpty()
    .withMessage('Company Type be not empty.'),
  body('android_url', 'Android Url must be specified.')
    .not()
    .isEmpty()
    .withMessage('Android Url must be not empty.'),
  body('ios_url', 'IOS Url must be specified.')
    .not()
    .isEmpty()
    .withMessage('IOS Url must be not empty.'),
  body('website_url', 'Website Url must be specified.')
    .not()
    .isEmpty()
    .withMessage('Website Url must be not empty.'),
  body('location_address', 'Location Address  must be specified.')
    .not()
    .isEmpty()
    .withMessage('Location Address must be not empty.'),

  body('latitude', 'Latitude must be specified.')
    .not()
    .isEmpty()
    .withMessage('Latitude must be not empty.')
    .isFloat()
    .withMessage('Latitude must be a valid latitude.'),

  body('longitude', 'Longitude must be specified.')
    .not()
    .isEmpty()
    .withMessage('Longitude must be not empty.')
    .isFloat()
    .withMessage('Longitude must be a valid latitude.'),
  body('key_features', 'Key Features must be specified.')
    .not()
    .isEmpty()
    .withMessage('Key Features must be not empty.')
    .isArray()
    .withMessage('Key Features must be a valid array.'),

  body('key_features', 'Key Features must be specified.')
    .not()
    .isEmpty()
    .withMessage('Key Features must be not empty.')
    .isArray()
    .withMessage('Key Features must be a valid array.'),

  body('documents', 'Documents must be specified.')
    .not()
    .isEmpty()
    .withMessage('Documents must be not empty.')
    .isArray()
    .withMessage('Documents must be a valid array.'),

  body(
    'payment_transfer_methods',
    'Payment Transfer Methods must be specified.'
  )
    .not()
    .isEmpty()
    .withMessage('Payment Transfer Methods must be not empty.')
    .isArray()
    .withMessage('Payment Transfer Methods must be a valid array.'),

  body('payment_methods', 'Payment Methods must be specified.')
    .not()
    .isEmpty()
    .withMessage('Payment Methods must be not empty.')
    .isArray()
    .withMessage('Payment Methods must be a valid array.'),

  body('name', 'Name must be specified.')
    .not()
    .isEmpty()
    .withMessage('Name must be not empty.'),

  body('phone', 'Phone must be specified.')
    .not()
    .isEmpty()
    .withMessage('Phone must be not empty.')
    .isNumeric()
    .withMessage('Phone must be a valid number.'),

  body('img', 'img must be specified.')
    .not()
    .isEmpty()
    .withMessage('img must be not empty.'),

  body('password', 'Password must be specified.')
    .not()
    .isEmpty()
    .withMessage('Password must be not empty.'),

  async (req, res) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        const error = errors.array();
        return res.status(400).json({
          status: false,
          statusCode: 400,
          message: error[0].msg,
          data: {},
        });
      }

      const partners = await Partners.register(
        new Partners({
          ...req.body,
          map_cordinates: [req.body.longitude, req.body.latitude],
        }),
        req.body.password
      );

      return res.status(201).json({
        status: true,
        statusCode: 201,
        message: 'partner created successfully',
        data: {
          partners,
        },
      });
    } catch (error) {
      return res.status(400).json({
        status: false,
        statusCode: 400,
        message: error.message,
        data: {},
      });
    }
  }
);

router.get('/:id', adminCheckToken, async (req, res) => {
  try {
    const partners = await Partners.findById(req.params.id);

    if (partners != null) {
      return res.status(200).json({
        status: true,
        statusCode: 200,
        message: 'partner get successfully',
        data: {
          partners,
        },
      });
    }

    return res.status(404).json({
      status: false,
      statusCode: 404,
      message: 'partner not found',
      data: {},
    });
  } catch (error) {
    return res.status(400).json({
      status: false,
      statusCode: 400,
      message: error.message,
      data: {},
    });
  }
});

router.put(
  '/:id',
  adminCheckToken,
  body('email', 'Email must be specified.')
    .not()
    .isEmpty()
    .withMessage('Email must be not empty.')
    .isEmail()
    .withMessage('Email must be a valid email address.'),
  body('about_exchange', 'About Exchange must be specified.')
    .not()
    .isEmpty()
    .withMessage('About Exchange must be not empty.'),
  body('company_type', 'Company Type must be specified.')
    .not()
    .isEmpty()
    .withMessage('Company Type be not empty.'),
  body('android_url', 'Android Url must be specified.')
    .not()
    .isEmpty()
    .withMessage('Android Url must be not empty.'),
  body('ios_url', 'IOS Url must be specified.')
    .not()
    .isEmpty()
    .withMessage('IOS Url must be not empty.'),
  body('website_url', 'Website Url must be specified.')
    .not()
    .isEmpty()
    .withMessage('Website Url must be not empty.'),
  body('location_address', 'Location Address  must be specified.')
    .not()
    .isEmpty()
    .withMessage('Location Address must be not empty.'),

  body('latitude', 'Latitude must be specified.')
    .not()
    .isEmpty()
    .withMessage('Latitude must be not empty.')
    .isFloat()
    .withMessage('Latitude must be a valid latitude.'),

  body('longitude', 'Longitude must be specified.')
    .not()
    .isEmpty()
    .withMessage('Longitude must be not empty.')
    .isFloat()
    .withMessage('Longitude must be a valid latitude.'),

  body('documents', 'Documents must be specified.')
    .not()
    .isEmpty()
    .withMessage('Documents must be not empty.')
    .isArray()
    .withMessage('Document must be a valid array.'),

  body(
    'payment_transfer_methods',
    'Payment Transfer Methods must be specified.'
  )
    .not()
    .isEmpty()
    .withMessage('Payment Transfer Methods must be not empty.')
    .isArray()
    .withMessage('Payment Transfer Methods must be a valid array.'),

  body('payment_methods', 'Payment Methods must be specified.')
    .not()
    .isEmpty()
    .withMessage('Payment Methods must be not empty.')
    .isArray()
    .withMessage('Payment Methods must be a valid array.'),

  body('name', 'Name must be specified.')
    .not()
    .isEmpty()
    .withMessage('Name must be not empty.'),

  body('phone', 'Phone must be specified.')
    .not()
    .isEmpty()
    .withMessage('Phone must be not empty.')
    .isNumeric()
    .withMessage('Phone must be a valid number.'),

  body('img', 'img must be specified.')
    .not()
    .isEmpty()
    .withMessage('img must be not empty.'),

  body('password', 'Password must be specified.')
    .not()
    .isEmpty()
    .withMessage('Password must be not empty.'),

  async (req, res) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        const error = errors.array();
        return res.status(400).json({
          status: false,
          statusCode: 400,
          message: error[0].msg,
          data: {},
        });
      }

      const partners = await Partners.findByIdAndUpdate(
        req.params.id,
        {
          ...req.body,
          map_cordinates: [req.body.longitude, req.body.latitude],
        },
        { new: true }
      );

      if (partners != null) {
        await partners.setPassword(req.body.password);

        return res.status(200).json({
          status: true,
          statusCode: 200,
          message: 'partner updated successfully',
          data: {
            partners,
          },
        });
      }

      return res.status(404).json({
        status: false,
        statusCode: 404,
        message: 'partner not found',
        data: {},
      });
    } catch (error) {
      return res.status(400).json({
        status: false,
        statusCode: 400,
        message: error.message,
        data: {},
      });
    }
  }
);

router.delete('/:id', adminCheckToken, async (req, res) => {
  try {
    const partners = await Partners.findByIdAndUpdate(req.params.id, {
      deletedAt: Date.now(),
    });

    if (partners != null) {
      return res.status(200).json({
        status: true,
        statusCode: 200,
        message: 'partner deleted successfully',
        data: {
          partners,
        },
      });
    }

    return res.status(404).json({
      status: false,
      statusCode: 404,
      message: 'partner not found',
      data: {},
    });
  } catch (error) {
    return res.status(400).json({
      status: false,
      statusCode: 400,
      message: error.message,
      data: {},
    });
  }
});

module.exports = router;
