const express = require('express');
const router = express.Router();
const Blogs = require('../../models/Blog');
const { adminCheckToken } = require('../../middlewear');
const { body, validationResult } = require('express-validator');

router.get('/', adminCheckToken, async (req, res) => {
  try {
    const blogs = await Blogs.find({}).lean();

    return res.status(200).json({
      status: true,
      statusCode: 200,
      message: 'get all blogs successfully',
      data: {
        blogs,
      },
    });
  } catch (error) {
    return res.status(400).json({
      status: false,
      statusCode: 400,
      message: error.message,
      data: {},
    });
  }
});

router.post(
  '/',
  adminCheckToken,
  body('title', 'Title must be specified.')
    .not()
    .isEmpty()
    .withMessage('Title must be not empty.'),

  body('subTitle', 'Sub Title must be specified.')
    .not()
    .isEmpty()
    .withMessage('Sub Title must be not empty.'),

  body('text', 'Text must be specified.')
    .not()
    .isEmpty()
    .withMessage('Text must be not empty.'),
  body('img', 'Image must be specified.')
    .not()
    .isEmpty()
    .withMessage('Image must be not empty.'),
  async (req, res) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        const error = errors.array();
        return res.status(400).json({
          status: false,
          statusCode: 400,
          message: error[0].msg,
          data: {},
        });
      }

      const blogs = await Blogs.create({
        title: req.body.title,
        subTitle: req.body.subTitle,
        text: req.body.text,
        img: req.body.img,
      });

      return res.status(201).json({
        status: true,
        statusCode: 201,
        message: 'blogs created successfully',
        data: {
          blogs,
        },
      });
    } catch (error) {
      return res.status(400).json({
        status: false,
        statusCode: 400,
        message: error.message,
        data: {},
      });
    }
  }
);

router.get('/:id', adminCheckToken, async (req, res) => {
  try {
    const blogs = await Blogs.findById(req.params.id);

    if (blogs != null) {
      res.status(200).json({
        status: true,
        statusCode: 200,
        message: 'blogs get successfully',
        data: {
          blogs,
        },
      });
    }

    res.status(404).json({
      status: false,
      statusCode: 404,
      message: 'blogs not found',
      data: {},
    });
  } catch (error) {
    return res.status(400).json({
      status: false,
      statusCode: 400,
      message: error.message,
      data: {},
    });
  }
});

router.put(
  '/:id',
  adminCheckToken,
  body('title', 'Title must be specified.')
    .not()
    .isEmpty()
    .withMessage('Title must be not empty.'),

  body('subTitle', 'Sub Title must be specified.')
    .not()
    .isEmpty()
    .withMessage('Sub Title must be not empty.'),

  body('text', 'Text must be specified.')
    .not()
    .isEmpty()
    .withMessage('Text must be not empty.'),
  body('img', 'Image must be specified.')
    .not()
    .isEmpty()
    .withMessage('Image must be not empty.'),
  async (req, res) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        const error = errors.array();
        return res.status(400).json({
          status: false,
          statusCode: 400,
          message: error[0].msg,
          data: {},
        });
      }

      const blogs = await Blogs.findByIdAndUpdate(
        req.params.id,
        {
          title: req.body.title,
          subTitle: req.body.subTitle,
          text: req.body.text,
          img: req.body.img,
        },
        { new: true }
      );

      if (blogs != null) {
        res.status(200).json({
          status: true,
          statusCode: 200,
          message: 'blogs updated successfully',
          data: {
            blogs,
          },
        });
      }

      res.status(404).json({
        status: false,
        statusCode: 404,
        message: 'blogs not found',
        data: {},
      });
    } catch (error) {
      return res.status(400).json({
        status: false,
        statusCode: 400,
        message: error.message,
        data: {},
      });
    }
  }
);

router.delete('/:id', adminCheckToken, async (req, res) => {
  try {
    const blogs = await Blogs.findByIdAndRemove(req.params.id);

    if (blogs != null) {
      res.status(200).json({
        status: true,
        statusCode: 200,
        message: 'blogs deleted successfully',
        data: {
          blogs,
        },
      });
    }

    res.status(404).json({
      status: false,
      statusCode: 404,
      message: 'blogs not found',
      data: {},
    });
  } catch (error) {
    return res.status(400).json({
      status: false,
      statusCode: 400,
      message: error.message,
      data: {},
    });
  }
});

module.exports = router;
