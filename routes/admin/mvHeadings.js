const express = require('express');
const router = express.Router();
const MVHeadings = require('../../models/mvHeading');
const { adminCheckToken } = require('../../middlewear');
const { body, validationResult } = require('express-validator');

router.get('/', adminCheckToken, async (req, res) => {
  try {
    const mvHeadings = await MVHeadings.findOne({}).lean();

    res.status(200).json({
      status: true,
      statusCode: 200,
      message: 'get our mission our vision data successfully',
      data: {
        mvHeadings,
      },
    });
  } catch (error) {
    return res.status(400).json({
      status: false,
      statusCode: 400,
      message: error.message,
      data: {},
    });
  }
});

router.post(
  '/',
  adminCheckToken,
  body('our_mission', 'Our Mission must be specified.')
    .not()
    .isEmpty()
    .withMessage('Our Mission be not empty.'),

  body('our_vision', 'Our Vision must be specified.')
    .not()
    .isEmpty()
    .withMessage('Our Vision be not empty.'),

  async (req, res) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        const error = errors.array();
        return res.status(400).json({
          status: false,
          statusCode: 400,
          message: error[0].msg,
          data: {},
        });
      }

      const mvHeadings = await MVHeadings.findOneAndUpdate({}, req.body, {
        new: true,
      });

      res.status(200).json({
        status: true,
        statusCode: 200,
        message: 'our mission our vision updated successfully',
        data: {
          mvHeadings,
        },
      });
    } catch (error) {
      return res.status(400).json({
        status: false,
        statusCode: 400,
        message: error.message,
        data: {},
      });
    }
  }
);

module.exports = router;
