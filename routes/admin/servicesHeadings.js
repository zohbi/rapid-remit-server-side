const express = require('express');
const router = express.Router();
const ServicesHeadings = require('../../models/servicesHeading');
const { adminCheckToken } = require('../../middlewear');
const { body, validationResult } = require('express-validator');

router.get('/', adminCheckToken, async (req, res) => {
  try {
    const servicesHeadings = await ServicesHeadings.find({}).lean();

    return res.status(200).json({
      status: true,
      statusCode: 200,
      message: 'get  all Services Headings successfully',
      data: {
        servicesHeadings,
      },
    });
  } catch (error) {
    return res.status(400).json({
      status: false,
      statusCode: 400,
      message: error.message,
      data: {},
    });
  }
});

router.post(
  '/',
  adminCheckToken,
  body('title', 'Title must be specified.')
    .not()
    .isEmpty()
    .withMessage('Title be not empty.'),

  body('text', 'Text must be specified.')
    .not()
    .isEmpty()
    .withMessage('Text be not empty.'),

  body('img', 'Image must be specified.')
    .not()
    .isEmpty()
    .withMessage('Image be not empty.'),
  async (req, res) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        const error = errors.array();
        return res.status(400).json({
          status: false,
          statusCode: 400,
          message: error[0].msg,
          data: {},
        });
      }

      const servicesHeadings = await ServicesHeadings.create({
        title: req.body.title,
        text: req.body.text,
        img: req.body.img,
      });
      return res.status(201).json({
        status: true,
        statusCode: 201,
        message: 'Services Heading created successfully',
        data: {
          servicesHeadings,
        },
      });
    } catch (error) {
      return res.status(400).json({
        status: false,
        statusCode: 400,
        message: error.message,
        data: {},
      });
    }
  }
);

router.get('/:id', adminCheckToken, async (req, res) => {
  try {
    const servicesHeadings = await ServicesHeadings.findById(req.params.id);

    if (servicesHeadings != null) {
      return res.status(200).json({
        status: false,
        statusCode: 200,
        message: 'Services Heading get successfully',
        data: {
          servicesHeadings,
        },
      });
    }
    return res.status(404).json({
      status: false,
      statusCode: 404,
      message: 'Services Heading not found',
      data: {},
    });
  } catch (error) {
    return res.status(400).json({
      status: false,
      statusCode: 400,
      message: error.message,
      data: {},
    });
  }
});

router.put(
  '/:id',
  adminCheckToken,
  body('title', 'Title must be specified.')
    .not()
    .isEmpty()
    .withMessage('Title be not empty.'),

  body('text', 'Text must be specified.')
    .not()
    .isEmpty()
    .withMessage('Text be not empty.'),

  body('img', 'Image must be specified.')
    .not()
    .isEmpty()
    .withMessage('Image be not empty.'),
  async (req, res) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        const error = errors.array();
        return res.status(400).json({
          status: false,
          statusCode: 400,
          message: error[0].msg,
          data: {},
        });
      }

      const servicesHeadings = await ServicesHeadings.findByIdAndUpdate(
        req.params.id,
        {
          title: req.body.title,
          text: req.body.text,
          img: req.body.img,
        },
        {
          new: true,
        }
      );
      if (servicesHeadings != null) {
        return res.status(200).json({
          status: true,
          statusCode: 200,
          message: 'Services Heading updated successfully',
          data: {
            servicesHeadings,
          },
        });
      }
      return res.status(404).json({
        status: false,
        statusCode: 404,
        message: 'Services Heading not found',
        data: {},
      });
    } catch (error) {
      return res.status(400).json({
        status: false,
        statusCode: 400,
        message: error.message,
        data: {},
      });
    }
  }
);

router.delete('/:id', adminCheckToken, async (req, res) => {
  try {
    const servicesHeadings = await ServicesHeadings.findByIdAndRemove(
      req.params.id
    );

    if (servicesHeadings != null) {
      return res.status(200).json({
        status: true,
        statusCode: 200,
        message: 'Services Heading deleted successfully',
        data: {
          servicesHeadings,
        },
      });
    }

    return res.status(404).json({
      status: false,
      statusCode: 404,
      message: 'Services Heading not found',
      data: {},
    });
  } catch (error) {
    return res.status(400).json({
      status: false,
      statusCode: 400,
      message: error.message,
      data: {},
    });
  }
});

module.exports = router;
