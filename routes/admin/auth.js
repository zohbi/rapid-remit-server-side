const express = require('express');
const router = express.Router();
const Admin = require('../../models/admin');
const middlewear = require('../../middlewear');
const { body, validationResult } = require('express-validator');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const config = require('../../config');

router.post(
  '/login',
  body('email', 'Email must be specified.')
    .not()
    .isEmpty()
    .withMessage('Email must be not empty.')
    .isEmail()
    .withMessage('Email must be a valid email address.'),
  body('password', 'Password must be specified.')
    .not()
    .isEmpty()
    .withMessage('password must be not empty.')
    .isLength({ min: 5 })
    .withMessage('Password must be 5 characters or greater.'),
  async (req, res) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        const error = errors.array();
        return res.status(400).json({
          status: false,
          statusCode: 400,
          message: error[0].msg,
          data: {},
        });
      }

      const admin = await Admin.findOne({
        email: req.body.email,
      }).lean();

      if (!admin) {
        return res.status(404).json({
          status: false,
          statusCode: 404,
          message: 'Admin not found',
          data: {},
        });
      }
      const areEqual = await bcrypt.compare(req.body.password, admin.password);
      if (!areEqual) {
        return res.status(401).json({
          status: false,
          statusCode: 401,
          message: 'Invalid credentials',
          data: {},
        });
      }

      const token = await jwt.sign({ id: admin._id }, config.adminSecret, {
        expiresIn: '90d',
      });

      return res.status(200).json({
        status: false,
        statusCode: 200,
        message: 'you are succesfully loged in',
        data: {
          admin,
          token,
        },
      });
    } catch (error) {
      return res.status(400).json({
        status: false,
        statusCode: 400,
        message: error.message,
        data: {},
      });
    }
  }
);

module.exports = router;
