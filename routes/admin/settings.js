const express = require('express');
const router = express.Router();
const Settings = require('../../models/setting');
const { adminCheckToken } = require('../../middlewear');
const { body, validationResult } = require('express-validator');

router.get('/', adminCheckToken, async (req, res) => {
  try {
    const setting = await Settings.findOne({}).lean();

    return res.status(200).json({
      status: true,
      statusCode: 200,
      message: 'get contact us data successfully',
      data: {
        setting,
      },
    });
  } catch (error) {
    return res.status(400).json({
      status: false,
      statusCode: 400,
      message: error.message,
      data: {},
    });
  }
});

router.post(
  '/',
  adminCheckToken,
  body('email', 'Email must be specified.')
    .not()
    .isEmpty()
    .withMessage('Email must be not empty.')
    .isEmail()
    .withMessage('Email must be a valid email address.'),

  body('companyName', 'Company Name must be specified.')
    .not()
    .isEmpty()
    .withMessage('Company Name be not empty.'),

  body('officeAddress', 'Office Address must be specified.')
    .not()
    .isEmpty()
    .withMessage('Office Address must be not empty.'),

  body('poBox', 'P.O.Box  must be specified.')
    .not()
    .isEmpty()
    .withMessage('P.O.Box must be not empty.'),

  body('city', 'City must be specified.')
    .not()
    .isEmpty()
    .withMessage('City must be not empty.'),

  body('country', 'Country must be specified.')
    .not()
    .isEmpty()
    .withMessage('Country must be not empty.'),

  body('latitude', 'Latitude must be specified.')
    .not()
    .isEmpty()
    .withMessage('Latitude must be not empty.')
    .isFloat()
    .withMessage('Latitude must be a valid latitude.'),

  body('longitude', 'Longitude must be specified.')
    .not()
    .isEmpty()
    .withMessage('Longitude must be not empty.')
    .isFloat()
    .withMessage('Longitude must be a valid latitude.'),

  body('facebookLink', 'Facebook Link must be specified.')
    .not()
    .isEmpty()
    .withMessage('Facebook Link must be not empty.'),

  body('instagramLink', 'Instagram In Link must be specified.')
    .not()
    .isEmpty()
    .withMessage('Instagram In Link must be not empty.'),

  body('linkedInLink', 'Linked In Link must be specified.')
    .not()
    .isEmpty()
    .withMessage('Linked In Link must be not empty.'),

  body('twitterLink', 'Twitter Link must be specified.')
    .not()
    .isEmpty()
    .withMessage('Twitter Link must be not empty.'),

  body('iosLink', 'IOS Link must be specified.')
    .not()
    .isEmpty()
    .withMessage('IOS Link must be not empty.'),

  body('androidLink', 'Android Link must be specified.')
    .not()
    .isEmpty()
    .withMessage('Android Link must be not empty.'),

  async (req, res) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        const error = errors.array();
        return res.status(400).json({
          status: false,
          statusCode: 400,
          message: error[0].msg,
          data: {},
        });
      }

      const setting = await Settings.findOneAndUpdate(
        {},
        {
          ...req.body,
          mapCoortinates: [req.body.longitude, req.body.latitude],
        },
        {
          new: true,
        }
      );

      return res.status(200).json({
        status: true,
        statusCode: 200,
        message: 'contactus updated successfully',
        data: {
          setting,
        },
      });
    } catch (error) {
      return res.status(400).json({
        status: false,
        statusCode: 400,
        message: error.message,
        data: {},
      });
    }
  }
);

module.exports = router;
