const express = require('express');
const router = express.Router();
const Offers = require('../../models/specialOffers');
const { adminCheckToken } = require('../../middlewear');
const { body, validationResult } = require('express-validator');

router.get('/', adminCheckToken, async (req, res) => {
  try {
    const offers = await Offers.find({}).lean();

    return res.status(200).json({
      status: true,
      statusCode: 200,
      message: 'get all offers successfully',
      data: {
        offers,
      },
    });
  } catch (error) {
    return res.status(400).json({
      status: false,
      statusCode: 400,
      message: error.message,
      data: {},
    });
  }
});

router.post(
  '/',
  adminCheckToken,
  body('title', 'Title must be specified.')
    .not()
    .isEmpty()
    .withMessage('Title must be not empty.'),

  body('text', 'Text must be specified.')
    .not()
    .isEmpty()
    .withMessage('Text must be not empty.'),

  body('expiryDate', 'Expiry Date must be specified.')
    .not()
    .isEmpty()
    .withMessage('Expiry Date must be not empty.')
    .isDate()
    .withMessage('Expiry Date Must be a valid date'),

  body('img', 'Image must be specified.')
    .not()
    .isEmpty()
    .withMessage('Image must be not empty.'),
  async (req, res) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        const error = errors.array();
        return res.status(400).json({
          status: false,
          statusCode: 400,
          message: error[0].msg,
          data: {},
        });
      }

      const offers = await Offers.create({
        title: req.body.title,
        text: req.body.text,
        expiryDate: req.body.expiryDate,
        img: req.body.img,
      });

      return res.status(201).json({
        status: true,
        statusCode: 201,
        message: 'offers created successfully',
        data: {
          offers,
        },
      });
    } catch (error) {
      return res.status(400).json({
        status: false,
        statusCode: 400,
        message: error.message,
        data: {},
      });
    }
  }
);

router.get('/:id', adminCheckToken, async (req, res) => {
  try {
    const offers = await Offers.findById(req.params.id);

    if (offers != null) {
      return res.status(200).json({
        status: true,
        statusCode: 200,
        message: 'offers get successfully',
        data: {
          offers,
        },
      });
    }

    return res.status(404).json({
      status: false,
      statusCode: 404,
      message: 'offers not found',
      data: {},
    });
  } catch (error) {
    return res.status(400).json({
      status: false,
      statusCode: 400,
      message: error.message,
      data: {},
    });
  }
});

router.put(
  '/:id',
  adminCheckToken,
  body('title', 'Title must be specified.')
    .not()
    .isEmpty()
    .withMessage('Title must be not empty.'),

  body('text', 'Text must be specified.')
    .not()
    .isEmpty()
    .withMessage('Text must be not empty.'),

  body('expiryDate', 'Expiry Date must be specified.')
    .not()
    .isEmpty()
    .withMessage('Expiry Date must be not empty.')
    .isDate()
    .withMessage('Expiry Date Must be a valid date'),

  body('img', 'Image must be specified.')
    .not()
    .isEmpty()
    .withMessage('Image must be not empty.'),
  async (req, res) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        const error = errors.array();
        return res.status(400).json({
          status: false,
          statusCode: 400,
          message: error[0].msg,
          data: {},
        });
      }

      const offers = await Offers.findByIdAndUpdate(
        req.params.id,
        {
          title: req.body.title,
          text: req.body.text,
          expiryDate: req.body.expiryDate,
          img: req.body.img,
        },
        { new: true }
      );

      if (offers != null) {
        return res.status(200).json({
          status: true,
          statusCode: 200,
          message: 'offers updated successfully',
          data: {
            offers,
          },
        });
      }

      return res.status(404).json({
        status: false,
        statusCode: 404,
        message: 'offers not found',
        data: {},
      });
    } catch (error) {
      return res.status(400).json({
        status: false,
        statusCode: 400,
        message: error.message,
        data: {},
      });
    }
  }
);

router.delete('/:id', adminCheckToken, async (req, res) => {
  try {
    const offers = await Offers.findByIdAndRemove(req.params.id);

    if (offers != null) {
      return res.status(200).json({
        status: true,
        statusCode: 200,
        message: 'offers deleted successfully',
        data: {
          offers,
        },
      });
    }

    return res.status(404).json({
      status: false,
      statusCode: 404,
      message: 'offers not found',
      data: {},
    });
  } catch (error) {
    return res.status(400).json({
      status: false,
      statusCode: 400,
      message: error.message,
      data: {},
    });
  }
});

module.exports = router;
