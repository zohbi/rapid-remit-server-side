const express = require('express');
const router = express.Router();
const PrivacyPolicyHeading = require('../../models/privacyPolicyHeading');
const { adminCheckToken } = require('../../middlewear');
const { body, validationResult } = require('express-validator');

router.get('/', adminCheckToken, async (req, res) => {
  try {
    const privacyPolicyHeading = await PrivacyPolicyHeading.findOne({}).lean();

    return res.status(200).json({
      status: true,
      statusCode: 200,
      message: 'get privacy policy heading data successfully',
      data: {
        privacyPolicyHeading,
      },
    });
  } catch (error) {
    return res.status(400).json({
      status: false,
      statusCode: 400,
      message: error.message,
      data: {},
    });
  }
});

router.post(
  '/',
  adminCheckToken,
  body('text', 'Text must be specified.')
    .not()
    .isEmpty()
    .withMessage('Text be not empty.'),

  async (req, res) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        const error = errors.array();
        return res.status(400).json({
          status: false,
          statusCode: 400,
          message: error[0].msg,
          data: {},
        });
      }

      const privacyPolicyHeadings = await PrivacyPolicyHeading.findOneAndUpdate(
        {},
        req.body,
        {
          new: true,
        }
      );

      return res.status(200).json({
        status: true,
        statusCode: 200,
        message: 'privacy policy heading updated successfully',
        data: {
          privacyPolicyHeadings,
        },
      });
    } catch (error) {
      return res.status(400).json({
        status: false,
        statusCode: 400,
        message: error.message,
        data: {},
      });
    }
  }
);

module.exports = router;
