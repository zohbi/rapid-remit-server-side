const express = require('express');
const router = express.Router();
const OurNetworkHeadings = require('../../models/ourNetworkHeading');
const { adminCheckToken } = require('../../middlewear');
const { body, validationResult } = require('express-validator');

router.get('/', adminCheckToken, async (req, res) => {
  try {
    const ourNetworkHeadings = await OurNetworkHeadings.findOne({}).lean();

    return res.status(200).json({
      status: true,
      statusCode: 200,
      message: 'get our network heading data successfully',
      data: {
        ourNetworkHeadings,
      },
    });
  } catch (error) {
    return res.status(400).json({
      status: false,
      statusCode: 400,
      message: error.message,
      data: {},
    });
  }
});

router.post(
  '/',
  adminCheckToken,
  body('heading', 'Heading must be specified.')
    .not()
    .isEmpty()
    .withMessage('Heading be not empty.'),

  body('text_1', 'Text One must be specified.')
    .not()
    .isEmpty()
    .withMessage('Text One be not empty.'),

  body('text_2', 'Text Two must be specified.')
    .not()
    .isEmpty()
    .withMessage('Text Two be not empty.'),

  async (req, res) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        const error = errors.array();
        return res.status(400).json({
          status: false,
          statusCode: 400,
          message: error[0].msg,
          data: {},
        });
      }

      const ourNetworkHeadings = await OurNetworkHeadings.findOneAndUpdate(
        {},
        req.body,
        {
          new: true,
        }
      );

      return res.status(200).json({
        status: true,
        statusCode: 200,
        message: 'our network heading updated successfully',
        data: {
          ourNetworkHeadings,
        },
      });
    } catch (error) {
      return res.status(400).json({
        status: false,
        statusCode: 400,
        message: error.message,
        data: {},
      });
    }
  }
);

module.exports = router;
