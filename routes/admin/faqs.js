const express = require('express');
const router = express.Router();
const Faq = require('../../models/faqs');
const { adminCheckToken } = require('../../middlewear');
const { body, validationResult } = require('express-validator');

router.get('/', adminCheckToken, async (req, res) => {
  try {
    const faqs = await Faq.find({}).lean();

    res.status(200).json({
      status: true,
      statusCode: 200,
      message: 'get all faqs successfully',
      data: {
        faqs,
      },
    });
  } catch (error) {
    return res.status(400).json({
      status: false,
      statusCode: 400,
      message: error.message,
      data: {},
    });
  }
});

router.post(
  '/',
  adminCheckToken,
  body('title', 'Title must be specified.')
    .not()
    .isEmpty()
    .withMessage('Title must be not empty.'),

  body('description', 'Description must be specified.')
    .not()
    .isEmpty()
    .withMessage('Description must be not empty.'),
  async (req, res) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        const error = errors.array();
        return res.status(400).json({
          status: false,
          statusCode: 400,
          message: error[0].msg,
          data: {},
        });
      }

      const faqs = await Faq.create({
        title: req.body.title,
        description: req.body.description,
      });

      res.status(201).json({
        status: true,
        statusCode: 201,
        message: 'faqs created successfully',
        data: {
          faqs,
        },
      });
    } catch (error) {
      return res.status(400).json({
        status: false,
        statusCode: 400,
        message: error.message,
        data: {},
      });
    }
  }
);

router.get('/:id', adminCheckToken, async (req, res) => {
  try {
    const faqs = await Faq.findById(req.params.id);

    if (faqs != null) {
      res.status(200).json({
        status: true,
        statusCode: 200,
        message: 'faqs get successfully',
        data: {
          faqs,
        },
      });
    }

    res.status(404).json({
      status: false,
      statusCode: 404,
      message: 'faqs not found',
      data: {},
    });
  } catch (error) {
    return res.status(400).json({
      status: false,
      statusCode: 400,
      message: error.message,
      data: {},
    });
  }
});
router.put(
  '/:id',
  adminCheckToken,
  body('title', 'Title must be specified.')
    .not()
    .isEmpty()
    .withMessage('Title must be not empty.'),

  body('description', 'Description must be specified.')
    .not()
    .isEmpty()
    .withMessage('Description must be not empty.'),
  async (req, res) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        const error = errors.array();
        return res.status(400).json({
          status: false,
          statusCode: 400,
          message: error[0].msg,
          data: {},
        });
      }

      const faqs = await Faq.findByIdAndUpdate(
        req.params.id,
        {
          title: req.body.title,
          description: req.body.description,
        },
        { new: true }
      );

      if (faqs != null) {
        return res.status(200).json({
          status: true,
          statusCode: 200,
          message: 'faqs updated successfully',
          data: {
            faqs,
          },
        });
      }

      return res.status(404).json({
        status: false,
        statusCode: 404,
        message: 'faqs not found',
        data: {},
      });
    } catch (error) {
      return res.status(400).json({
        status: false,
        statusCode: 400,
        message: error.message,
        data: {},
      });
    }
  }
);

router.delete('/:id', adminCheckToken, async (req, res) => {
  try {
    const faqs = await Faq.findByIdAndRemove(req.params.id);

    if (faqs != null) {
      res.status(200).json({
        status: true,
        statusCode: 200,
        message: 'faqs deleted successfully',
        data: {
          faqs,
        },
      });
    }

    res.status(404).json({
      status: false,
      statusCode: 404,
      message: 'faqs not found',
      data: {},
    });
  } catch (error) {
    return res.status(400).json({
      status: false,
      statusCode: 400,
      message: error.message,
      data: {},
    });
  }
});

module.exports = router;
