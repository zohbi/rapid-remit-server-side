const express = require('express');
const router = express.Router();
const DoHeadings = require('../../models/doHeading');
const { adminCheckToken } = require('../../middlewear');
const { body, validationResult } = require('express-validator');

router.get('/', adminCheckToken, async (req, res) => {
  try {
    const doHeadings = await DoHeadings.findOne({}).lean();

    res.status(200).json({
      status: true,
      statusCode: 200,
      message: 'get what we do heading data successfully',
      data: {
        doHeadings,
      },
    });
  } catch (error) {
    return res.status(400).json({
      status: false,
      statusCode: 400,
      message: error.message,
      data: {},
    });
  }
});

router.post(
  '/',
  adminCheckToken,
  body('heading', 'Heading must be specified.')
    .not()
    .isEmpty()
    .withMessage('Heading be not empty.'),

  body('sub_heading', 'Sub Heading must be specified.')
    .not()
    .isEmpty()
    .withMessage('Sub Heading be not empty.'),

  body('text', 'Text must be specified.')
    .not()
    .isEmpty()
    .withMessage('Text be not empty.'),

  async (req, res) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        const error = errors.array();
        return res.status(400).json({
          status: false,
          statusCode: 400,
          message: error[0].msg,
          data: {},
        });
      }

      const doHeadings = await DoHeadings.findOneAndUpdate({}, req.body, {
        new: true,
      });

      res.status(200).json({
        status: true,
        statusCode: 200,
        message: 'what we do heading updated successfully',
        data: {
          doHeadings,
        },
      });
    } catch (error) {
      return res.status(400).json({
        status: false,
        statusCode: 400,
        message: error.message,
        data: {},
      });
    }
  }
);

module.exports = router;
