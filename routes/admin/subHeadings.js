const express = require('express');
const router = express.Router();
const SubHeadings = require('../../models/subHeading');
const { adminCheckToken } = require('../../middlewear');
const { body, validationResult } = require('express-validator');

router.get('/', adminCheckToken, async (req, res) => {
  try {
    const subHeadings = await SubHeadings.findOne({}).lean();

    return res.status(200).json({
      status: true,
      statusCode: 200,
      message: 'get Sub Headings data successfully',
      data: {
        subHeadings,
      },
    });
  } catch (error) {
    return res.status(400).json({
      status: false,
      statusCode: 400,
      message: error.message,
      data: {},
    });
  }
});

router.post(
  '/',
  adminCheckToken,
  body('para_1', 'Paragraph One must be specified.')
    .not()
    .isEmpty()
    .withMessage('Paragraph One be not empty.'),

  body('para_1', 'Paragraph Two must be specified.')
    .not()
    .isEmpty()
    .withMessage('Paragraph Two be not empty.'),

  async (req, res) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        const error = errors.array();
        return res.status(400).json({
          status: false,
          statusCode: 400,
          message: error[0].msg,
          data: {},
        });
      }

      const subHeadings = await SubHeadings.findOneAndUpdate({}, req.body, {
        new: true,
      });

      res.status(200).json({
        status: true,
        statusCode: 200,
        message: 'Sub Headings updated successfully',
        data: {
          subHeadings,
        },
      });
    } catch (error) {
      return res.status(400).json({
        status: false,
        statusCode: 400,
        message: error.message,
        data: {},
      });
    }
  }
);

module.exports = router;
