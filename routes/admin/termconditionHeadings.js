const express = require('express');
const router = express.Router();
const TermConditionHeadings = require('../../models/termConditionHeading');
const { adminCheckToken } = require('../../middlewear');
const { body, validationResult } = require('express-validator');

router.get('/', adminCheckToken, async (req, res) => {
  try {
    const termConditionHeadings = await TermConditionHeadings.findOne(
      {}
    ).lean();

    return res.status(200).json({
      status: true,
      statusCode: 200,
      message: 'get term condition heading data successfully',
      data: {
        termConditionHeadings,
      },
    });
  } catch (error) {
    return res.status(400).json({
      status: false,
      statusCode: 400,
      message: error.message,
      data: {},
    });
  }
});

router.post(
  '/',
  adminCheckToken,
  body('text', 'Text must be specified.')
    .not()
    .isEmpty()
    .withMessage('Text be not empty.'),

  async (req, res) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        const error = errors.array();
        return res.status(400).json({
          status: false,
          statusCode: 400,
          message: error[0].msg,
          data: {},
        });
      }

      const termConditionHeadings = await TermConditionHeadings.findOneAndUpdate(
        {},
        req.body,
        {
          new: true,
        }
      );

      return res.status(200).json({
        status: true,
        statusCode: 200,
        message: 'term condition heading updated successfully',
        data: {
          termConditionHeadings,
        },
      });
    } catch (error) {
      return res.status(400).json({
        status: false,
        statusCode: 400,
        message: error.message,
        data: {},
      });
    }
  }
);

module.exports = router;
