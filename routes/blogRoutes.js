const express = require('express');
const Blog = require('../models/Blog');
const router = express.Router();

router
  .route('/')
  .get((req, res, next) => {
    Blog.find({})
      .then(
        (resp) => {
          res.statusCode = 200;
          res.setHeader('Content-Type', 'application/json');
          res.json(resp);
        },
        (err) => next(err)
      )
      .catch((err) => next(err));
  })
  .post((req, res, next) => {
    Blog.create(req.body)
      .then(
        (news) => {
          console.log('News Created ', news);
          res.statusCode = 200;
          res.setHeader('Content-Type', 'application/json');
          res.json(news);
        },
        (err) => next(err)
      )
      .catch((err) => next(err));
  });

router.route('/:Id').get((req, res, next) => {
  Blog.findById(req.params.Id)
    .then(
      (resp) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(resp);
      },
      (err) => next(err)
    )
    .catch((err) => next(err));
});

module.exports = router;
