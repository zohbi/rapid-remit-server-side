const express = require('express');
const Offers = require('../models/specialOffers');
const router = express.Router();

router.route('/').get((req, res, next) => {
  Offers.find({})
    .then(
      (resp) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(resp);
      },
      (err) => next(err)
    )
    .catch((err) => next(err));
});

module.exports = router;
