const express = require("express");
const Count = require("../models/counts");
const Rate = require("../models/rates");
const Partners = require("../models/partners");
const User = require("../models/user");

const router = express.Router();

router.route("/").post((req, res, next) => {
  Count.create(req.body)
    .then(
      (resp) => {
        res.statusCode = 200;
        res.setHeader("Content-Type", "application/json");
        res.json(resp);
      },
      (err) => next(err)
    )
    .catch((err) => next(err));
});

router.route("/").get(async (req, res, next) => {
  let transaction_count = await Count.findOne();
  let partners_count = await Partners.countDocuments();
  let countries_count = await Rate.find({ partner: "moneygram" });
  let user_count = await User.countDocuments();

  console.log(
    "new",
    transaction_count.rates_count,
    partners_count,
    countries_count.length,
    user_count
  );
  res.status(200).json({
    partners_count,
    user_count,
    countries_count: countries_count.length,
    transaction_count: transaction_count.rates_count,
  });
});

// router.put("/:id", async (req, res) => {
//   Content.findByIdAndUpdate(
//     req.params.id,
//     {
//       $set: {
//         comparison_one: req.body.comparison_one,
//         comparison_two: req.body.comparison_two,
//       },
//     },
//     { new: true }
//   ).then((resp) => {
//     res.statusCode = 200;
//     res.setHeader("Content-Type", "application/json");
//     res.json(resp);
//   });
// });

module.exports = router;
