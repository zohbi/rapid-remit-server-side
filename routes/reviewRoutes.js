const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const Joi = require('@hapi/joi');

const Review = require('../models/reviews');
const Partner = require('../models/partners');

const router = express.Router();

//router.use(bodyParser.json());

router
  .route('/')
  .get((req, res, next) => {
    //console.log('Requested by', req.user);
    Review.find({})
      .populate('user_id', 'image')
      .then(
        (news) => {
          res.statusCode = 200;
          res.setHeader('Content-Type', 'application/json');
          res.json(news);
        },
        (err) => next(err)
      )
      .catch((err) => next(err));
  })
  .post((req, res, next) => {
    const data = req.body;
    const schema = Joi.object().keys({
      name: Joi.string().required(),
      text: Joi.string().min(3),
      img: Joi.string(),
      partner_id: Joi.string().required(),
      user_id: Joi.string().required(),
      rating: Joi.number(),
    });
    const val = schema.validate(data);
    if (!val.error) {
      Review.findOne({
        partner_id: data.partner_id,
        user_id: data.user_id,
      }).then((partner) => {
        console.log('partner', partner);
        //console.log("data" ,partner )
        if (partner) {
          res.statusCode = 403;
          res.setHeader('Content-Type', 'application/json');
          res.json({ error: true, message: 'already exsist' });
        } else {
          Review.create(data)
            .then(
              (review) => {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(review);
              },
              (err) => next(err)
            )
            .catch((err) => next(err));
        }
      });
    } else {
      res.statusCode = 400;
      res.setHeader('Content-Type', 'application/json');
      res.json({ error: true, message: val.error });
    }
  })
  .delete((req, res, next) => {
    Review.remove({})
      .then(
        (resp) => {
          res.statusCode = 200;
          res.setHeader('Content-Type', 'application/json');
          res.json(resp);
        },
        (err) => next(err)
      )
      .catch((err) => next(err));
  });

router
  .route('/:newsId')
  // .get((req, res, next) => {
  //   var u = req.user._id;

  //   Review.findById(req.params.newsId)
  //     .then(
  //       (news) => {
  //         res.statusCode = 200;
  //         res.setHeader("Content-Type", "application/json");
  //         res.json(news);
  //       },
  //       (err) => next(err)
  //     )
  //     .catch((err) => next(err));
  // })
  .delete((req, res, next) => {
    News.findByIdAndRemove(req.params.newsId)
      .then(
        (resp) => {
          res.statusCode = 200;
          res.setHeader('Content-Type', 'application/json');
          res.json(resp);
        },
        (err) => next(err)
      )
      .catch((err) => next(err));
  });

router.route('/id/top').get((req, res, next) => {
  Review.findOne({})
    .sort({ _id: -1 })
    .then(
      (news) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(news);
      },
      (err) => next(err)
    )
    .catch((err) => next(err));
});

router.get('/user_id/:id', async (req, res, next) => {
  Review.find({ user_id: req.params.id })
    .then(
      (resp) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(resp);
      },
      (err) => next(err)
    )
    .catch((err) => next(err));
});

router.put('/:id', async (req, res, next) => {
  Review.findByIdAndUpdate(req.params.id, { $set: req.body }, { new: true })
    .then(
      (resp) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(resp);
      },
      (err) => next(err)
    )
    .catch((err) => next(err));
});

router.get('/partner', async (req, res, next) => {
  let partner = await Partner.find();
  let ratings = {};
  for (let i = 0; i < partner.length; i++) {
    let review = await Review.find({ partner_id: partner[i]._id });
    let sum = 0;
    //console.log("review", review);
    review.map((s) => {
      sum = sum + s.rating;
    });
    let average = sum / review.length;
    ratings[partner[i].name] = average;
    console.log('ratings', ratings);
  }

  res.statusCode = 200;
  res.setHeader('Content-Type', 'application/json');
  res.json(ratings);
});

module.exports = router;
