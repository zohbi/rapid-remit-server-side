const express = require('express');
const ServicesHeadings = require('../models/servicesHeading');
const router = express.Router();

router.route('/').get((req, res, next) => {
  ServicesHeadings.find({})
    .then(
      (resp) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(resp);
      },
      (err) => next(err)
    )
    .catch((err) => next(err));
});

module.exports = router;
