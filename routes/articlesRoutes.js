const express = require('express');
const Articles = require('../models/Articles');
const router = express.Router();

router.route('/').get((req, res, next) => {
  Articles.find({})
    .then(
      (resp) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(resp);
      },
      (err) => next(err)
    )
    .catch((err) => next(err));
});

router.route('/:Id').get((req, res, next) => {
  Articles.findById(req.params.Id)
    .then(
      (resp) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(resp);
      },
      (err) => next(err)
    )
    .catch((err) => next(err));
});

module.exports = router;
