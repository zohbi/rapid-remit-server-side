var express = require("express");
const bodyParser = require("body-parser");
var User = require("../models/superUser");
var Config = require("../models/configSchema");

const mongoose = require("mongoose");
//var app = express();

var router = express.Router();
//console.log('users connectes');
router.use(bodyParser.json());
let jwt = require("jsonwebtoken");
//router.use(express.json());
var mkdirp = require("mkdirp");
const config = require("../config");

router.post("/signup", async (req, res) => {
  console.log("start");

  const { email } = req.body;
  const existUser = await User.findOne({ email });

  let _a;
  if (!existUser) {
    _a = new User({
      email: req.body.email,

      username: req.body.username,

      password: req.body.password,
    });

    let user = await _a.save();
    let token = jwt.sign({ email: req.body.email }, config.secret, {
      expiresIn: "24h", // expires in 24 hours
    });

    if (user.err) {
      res.code(400).send({ val: _a.err, error: true });
      //return { val: _a.err, error: true };
    } else {
      //ssport.authenticate("local");
      // if (auth) {

      user.token = await token;
      // user.hashPassword = undefined;
      // user.salt = undefined;
      res.setHeader("Content-Type", "application/json");
      res.statusCode = 200;
      res.json({
        success: true,
        status: "Registration Successful!",
        data: user,
        token: token,
      });
    }
    // res.json({
    //   success: true,
    //   status: "Registration Successful!",
    //   data: user,
    //   token: token
    // });
  } else {
    res.setHeader("Content-Type", "application/json");
    res.statusCode = 400;
    res.json({
      success: false,
      status: "Email already exsists",
    });
  }
});

// router.get("/", async (req, res) => {
//   let users = await User.find({}, { hash: 0, salt: 0, hashPassword: 0 });
//   console.log("Users", users);
//   res.setHeader("Content-Type", "application/json");
//   res.statusCode = 200;
//   res.json(users);
// });

// router.put("/changeStatus/:id", async (req, res) => {
//   let id = req.params.id;
//   let users = await User.findById(id);
//   let status = !users.status;
//   let update = await User.findByIdAndUpdate(
//     id,
//     {
//       $set: { status: status }
//     },
//     { new: true }
//   );

//   console.log("Users", users, update);
//   res.setHeader("Content-Type", "application/json");
//   res.statusCode = 200;
//   res.json({ status: update.status });
// });

// router.post("/signup", (req, res, next) => {
//   if (req.body.password.length < 8) {
//     console.log(req.body.password.length);
//     res.statusCode = 500;
//     res.json({ err: "password must have atleast 8 chracters" });
//     console.log("password must have atleast 8 chracters");
//   }
//   if (req.body.password != req.body.conformPassword) {
//     res.setHeader("Content-Type", "application/json");
//     res.statusCode = 500;
//     res.json({ err: "password does not matched" });
//     console.log("password does not matched");
//   } else {
//     if (!req.body.admin) {
//       req.body.admin = false;
//     }
//     User.register(
//       new User({
//         email: req.body.email,
//         streetAddress: req.body.streetAddress,
//         address: req.body.address,
//         firstName: req.body.firstName,
//         lastName: req.body.lastName,
//         country: req.body.country,
//         city: req.body.city,
//         phone: req.body.phone,
//         role: req.body.role,
//         admin: req.body.admin
//       }),
//       req.body.password,
//       (err, user) => {
//         console.log("Data", user);
//         console.log("err", err);

//         if (err) {
//           res.statusCode = 500;
//           res.setHeader("Content-Type", "application/json");
//           res.json({ err: err });
//         } else {
//           passport.authenticate("local");
//           res.statusCode = 200;
//           res.setHeader("Content-Type", "application/json");

//           mkdirp("./public/" + user.email, function(err) {
//             if (err) console.error(err);
//             else console.log("pow!");
//           });

//           res.json({
//             success: true,
//             status: "Registration Successful!",
//             data: user
//           });
//         }
//       }
//     );
//   }
// });

router.post("/config", async (req, res) => {
  //checkforvaliudation
  let data = req.body;

  if (!data.key || !data.title || !data.content) {
    res.setHeader("Content-Type", "application/json");
    res.statusCode = 400;
    res.json({ success: false, error: "inavlid entry." });
  }

  //checkforvaliudation

  const _config = await Config.findOne({ key: data.key });
  if (_config) {
    await _config.remove();
  }

  const setConfig = new Config({ ...data });
  await setConfig.save();
  res.setHeader("Content-Type", "application/json");
  res.statusCode = 200;
  res.json({ ...setConfig._doc, success: true });
});

router.get("/config/:key", async (req, res) => {
  //checkforvaliudation
  // let data = req.body;

  let key = req.params.key;
  if (!key) {
    res.setHeader("Content-Type", "application/json");
    res.statusCode = 400;
    res.json({ success: false, error: "inavlid entry." });
  }

  //checkforvaliudation

  const _config = await Config.findOne({ key: key });
  if (_config) {
    res.setHeader("Content-Type", "application/json");
    res.statusCode = 200;
    res.json({ success: true, response: _config });
  } else {
    res.setHeader("Content-Type", "application/json");
    res.statusCode = 400;
    res.json({ success: false, error: "inavlid entry." });
  }
});

router.post("/login", (req, res, err) => {
  let data = req.body;
  User.findOne({ email: data.email }).then((user) => {
    console.log("user", user, data);
    if (!user) {
      return res.status(404).json({ error: "User not found" });
    }
    if (!user.auth(data.password)) {
      console.log("---", user);
      return res.status(401).json({ error: "Password incorrect" });
    }

    //var token = auth.getToken({ _id: user._id });

    let token = jwt.sign({ email: data.email }, config.secret, {
      expiresIn: "24h", // expires in 24 hours
    });
    // return the JWT token for the future API calls
    // res.json({
    //   success: true,
    //   message: 'Authentication successful!',
    //   token: token
    // });

    // user.hashPassword = undefined;
    // user.salt = undefined;
    res.setHeader("Content-Type", "application/json");
    res.statusCode = 200;
    res.json({
      success: true,
      token: token,
      status: "you are succesfully loged in",
      data: user,
    });
  });
});
module.exports = router;
