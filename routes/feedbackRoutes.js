const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const middlewear = require("../middlewear");

const FeedBack = require("../models/Feedbacks");
const Profile = require("../models/Profile");

const router = express.Router();

router.route("/").get((req, res, next) => {
  FeedBack.find({})
    .then(
      resp => {
        res.statusCode = 200;
        res.setHeader("Content-Type", "application/json");
        res.json(resp);
      },
      err => next(err)
    )
    .catch(err => next(err));
});

router.post("/:id",  (req, res, next) => {
  req.body.author = req.params.id;

  Profile.findOne({ author: req.params.id }).then(i => {
    i && i.img ? (req.body.img = i.img) : null;
    console.log(req.body.img);
    FeedBack.create(req.body).then(resp => {
      res.statusCode = 200;
      res.setHeader("Content-Type", "application/json");
      res.json(resp);
    });
  }),
    err => next(err).catch(err => next(err));
});

// router.route('/id/top')
// .get((req,res,next) => {

//     FeedBack.findOne({}).sort({"_id":-1})
//     .then((resp) => {
//         res.statusCode = 200;
//         res.setHeader('Content-Type', 'application/json');
//         res.json(resp);
//     }, (err) => next(err))
//     .catch((err) => next(err));
// })

module.exports = router;
