const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const Rates = require('../models/rates');
const currency = require('../models/currency');
const axios = require('axios');
const Log = require('../models/rateLoges');
const DATEOBJ = require('date-and-time');
const io = require('socket.io-client');
var socket = io.connect('http://localhost:4000/');

const router = express.Router();

router.use(bodyParser.json());

router.get('/', async (req, res, next) => {
  try {
    let currencies = await currency.find({});

    res.statusCode = 200;
    res.setHeader('Content-Type', 'application/json');
    res.json(currencies);
  } catch (e) {
    console.log(e);
  }
});

module.exports = router;
