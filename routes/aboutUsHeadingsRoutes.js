const express = require('express');
const AboutUsHeadings = require('../models/aboutUsHeading');
const router = express.Router();

router.route('/').get((req, res, next) => {
  AboutUsHeadings.findOne({})
    .then(
      (resp) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(resp);
      },
      (err) => next(err)
    )
    .catch((err) => next(err));
});

module.exports = router;
