const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const Promotions = require('../models/promotions');
const multer = require('multer');
const middlewear = require('../middlewear');

const storage2 = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, 'public/promotionImages');
  },

  filename: (req, file, cb) => {
    cb(null, file.originalname);
  },
});

const imageFileFilter = (req, file, cb) => {
  if (!file.originalname.match(/\.(JPG|jpeg|png|gif|jpg)$/)) {
    return cb(new Error('You can upload only image files!'), false);
  }
  cb(null, true);
};
//const upload = multer({ storage: storage, fileFilter: imageFileFilter });
const imageUpload = multer({ storage: storage2 });

const router = express.Router();

router.get('/', (req, res, next) => {
  console.log('promo', new Date(2020, 3, 23));
  Promotions.find({
    expiryDate: { $gte: new Date() },
    start_date: { $lte: new Date() },
  })
    .then(
      (resp) => {
        console.log('resp', resp);
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(resp);
      },
      (err) => next(err)
    )
    .catch((err) => next(err));
});

router.get('/:partner_id', (req, res, next) => {
  Promotions.find({ partner_id: req.params.partner_id })
    .then(
      (resp) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(resp);
      },
      (err) => next(err)
    )
    .catch((err) => next(err));
});

router.put('/click/:Id', (req, res, next) => {
  req.body.avail = true;
  Promotions.findByIdAndUpdate(
    req.params.Id,
    {
      $set: req.body.avail,
    },
    { new: true }
  )
    .then(
      (resp) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(resp);
      },
      (err) => next(err)
    )
    .catch((err) => next(err));
});
var uploadImage = imageUpload.single('image', (err, done) => {
  console.log('uploaded');
  if (err) {
    console.log(err);
  }
});

router.post('/upload', uploadImage, async (req, res) => {
  console.log('upload', req.file);
  if (req.file) {
    console.log('upload', req.file);
    res.status(200).json(req.file.path);
  }
});

router.post('/', (req, res, next) => {
  console.log('starty', req.body);
  //req.body.expiryDate = new Date(req.body.expiryDate);

  Promotions.create(req.body)
    .then(
      (resp) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(resp);
      },
      (err) => next(err)
    )
    .catch((err) => next(err));
});

router.post('/click', (req, res, next) => {
  //req.body.expiryDate = new Date(req.body.expiryDate);
  const data = req.body;

  Promotions.findByIdAndUpdate(
    data.promotion_id,
    {
      $push: { users: data.user_id },
    },
    { new: true }
  )
    .then(
      (resp) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(resp);
      },
      (err) => next(err)
    )
    .catch((err) => next(err));
});

function del(req, res, next) {
  Promotions.findOne({ _id: req.params._id }).then((pic) => {
    fs.unlink('./public/Promotions/' + pic.img, (err) => {
      if (err) console.log(err);
      console.log('successfully deleted image');
    });
  });
}

router.route('/promotions/:Id').get((req, res, next) => {
  Promotions.findById(req.params.Id)
    .then(
      (resp) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(resp);
      },
      (err) => next(err)
    )
    .catch((err) => next(err));
});

router.delete('/promotions/:Id', (req, res, next) => {
  Promotions.findOne({ _id: req.params.Id }).then((pic) => {
    console.log('null', pic);
    fs.unlink('./public/Promotions/' + pic.img, (err) => {
      if (err) console.log(err);
      console.log('deleted');
    });
  });
  Promotions.findByIdAndRemove(req.params.Id)
    .then(
      (resp) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json({ data: resp, message: 'you have deleted a promotion' });
      },
      (err) => next(err)
    )
    .catch((err) => next(err));
});

router.put('/:Id', (req, res, next) => {
  console.log('p');
  Promotions.findByIdAndUpdate(
    req.params.Id,
    {
      $set: req.body,
    },
    { new: true }
  )
    .then(
      (resp) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json({
          data: resp,
          message: 'you have updated a particular promotions',
        });
      },
      (err) => next(err)
    )
    .catch((err) => next(err));
});

module.exports = router;
