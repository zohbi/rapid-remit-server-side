const express = require('express');

const Content = require('../models/content');

const router = express.Router();

router.route('/').post((req, res, next) => {
  Content.create(req.body)
    .then(
      (resp) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(resp);
      },
      (err) => next(err)
    )
    .catch((err) => next(err));
});

router.route('/').get((req, res, next) => {
  Content.find()
    .then(
      (resp) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(resp);
      },
      (err) => next(err)
    )
    .catch((err) => next(err));
});

router.put('/:id', async (req, res) => {
  Content.findByIdAndUpdate(
    req.params.id,
    {
      $set: {
        comparison_one: req.body.comparison_one,
        comparison_two: req.body.comparison_two,
      },
    },
    { new: true }
  ).then((resp) => {
    res.statusCode = 200;
    res.setHeader('Content-Type', 'application/json');
    res.json(resp);
  });
});

module.exports = router;
