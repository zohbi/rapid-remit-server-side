const express = require('express');
const SubHeadings = require('../models/subHeading');
const router = express.Router();

router.route('/').get((req, res, next) => {
  SubHeadings.findOne({})
    .then(
      (resp) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(resp);
      },
      (err) => next(err)
    )
    .catch((err) => next(err));
});

module.exports = router;
